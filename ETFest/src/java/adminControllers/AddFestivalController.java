/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminControllers;

import com.mysql.jdbc.StringUtils;
import dataModels.FestivalModel;
import entity.AvailableTickets;
import entity.Festival;
import entity.Performance;
import entity.Performer;
import entity.SocialLink;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.inject.Named;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Petar
 */
@Named
@SessionScoped
public class AddFestivalController implements Serializable {

    public static final String VIDEO_FOLDER_PATH = "F:\\Projekti\\PIA\\pia\\ETFest\\build\\web\\resources\\videos\\";
    private static final int AUTOCOMPLETE_THRESHOLD = 2;
    private static final int MAX_AUTOCOMPLETE_SUGGESTIONS = 8;
    public static final String CREATE_MESSAGE_ATTRIBUTE_NAME = "createMessage";

    private int activeStep = 0;

    private final String[] steps = new String[]{"Basic information", "Schedule", "Gallery"};

    private FestivalModel model;

    private String errorMessage;

    /**
     * Creates a new instance of AddFestivalController
     */
    public AddFestivalController() {
        resetModelData();
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public int getActiveStep() {
        return activeStep;
    }

    public void setActiveStep(int activeStep) {
        this.activeStep = activeStep;
    }

    public String currentStep() {
        return steps[activeStep];
    }

    public String getStep(int index) {
        return steps[index];
    }

    public void schedule(ActionEvent actionEvent) {
        if (validateInputLevelOne()) {
            activeStep = 1;
            FacesContext context = FacesContext.getCurrentInstance();
            context.getApplication().getNavigationHandler().handleNavigation(context, null, "addFestivalSchedule?faces-redirect=true");
        }
    }

    public void gallery(ActionEvent actionEvent) {
        activeStep = 2;
        FacesContext context = FacesContext.getCurrentInstance();
        context.getApplication().getNavigationHandler().handleNavigation(context, null, "addFestivalGallery?faces-redirect=true");
    }

    public void save() {
        if (!validateInputLevelOne()) {
            return;
        }
        if (!validatePerformanceDates()) {
            return;
        }
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            Festival festival = persistBasicInfo(session);
            Hibernate.initialize(festival.getPerformances());
            persistPerformances(session, festival);
            persistFiles(session, festival);
            tx.commit();
            session.flush();
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
            errorMessage = "There has been an error. Please try again.";
            return;
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
            errorMessage = "There has been an error. Please try again.";
            return;
        } finally {
            if (null != session) {
                session.close();
            }
        }

        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(
                null, new FacesMessage("Success!", "You have successfully added festival " + model.getName()));
        context.getExternalContext()
                .getFlash().setKeepMessages(true);

        resetModelData();
        context.getApplication()
                .getNavigationHandler().handleNavigation(context, null, "adminHome?faces-redirect=true");
    }

    private Festival persistBasicInfo(Session session) {
        Festival festival = new Festival(model.getName(),
                util.DateUtil.dateDayDiff(model.getStartDate(), model.getEndDate()),
                model.getStartDate(),
                model.getEndDate(),
                model.getLocation(),
                model.getPricePerDay(),
                model.getPriceWholeFestival(),
                model.getMaxTicketsPerUser(),
                0 /*views*/,
                false /*isCanceled*/);

        session.persist(festival);
        model.getSelectedSocialMedia().forEach((socialMedia) -> {
            SocialLink link = new SocialLink();
            String actualLink = model.getSocialMediaLinks().get(socialMedia.getName());
            if (!(StringUtils.isNullOrEmpty(actualLink))) {
                link.setFestival(festival);
                link.setLink(actualLink);
                link.setNetwork(socialMedia.getName());
                session.persist(link);
                festival.getSocialLinks().add(link);
            }
        });

        int numDays = util.DateUtil.dateDayDiff(festival.getStartDate(), festival.getEndDate()) + 1;
        for (int i = 1; i <= numDays; i++) {
            AvailableTickets tickets = new AvailableTickets(festival, i, model.getNumTicketsPerDay(), model.getNumTicketsPerDay());

            session.persist(tickets);
            festival.getAvailableTicketses().add(tickets);
        }

        return festival;
    }

    private void persistPerformances(Session session, Festival festival) {
        List<Performer> existingPerformers = session.createCriteria(Performer.class).list();

        for (Performance performance : model.getPerformances()) {
            if (!existingPerformers.stream().anyMatch((performer) -> performer.getName().equals(performance.getPerformer().getName()))) {
                session.persist(performance.getPerformer());
                session.flush();
            }
            performance.setFestival(festival);
            festival.getPerformances().add(performance);
            Performer performer = performance.getPerformer();
            performer = (Performer) session.get(Performer.class, performer.getId());
            Hibernate.initialize(performer.getPerformances());
            performer.getPerformances().add(performance);
            session.persist(performance);
        }
    }

    private void persistFiles(Session session, Festival festival) {

        for (UploadedFile uploadedFile : model.getUploadedFiles()) {
            util.FileUtil.persistFile(session, festival, uploadedFile, null);
        }
    }

    public void goToStep(int index) {
        this.activeStep = index;
    }

    public FestivalModel getModel() {
        return model;
    }

    public List<String> completeFestivalName(String query) {
        if (query.length() < AUTOCOMPLETE_THRESHOLD) {
            return null;
        }
        List<String> results = new ArrayList<>();
        List<Festival> existingFestivals = model.getExistingFestivals();

        existingFestivals.stream().filter((fest) -> (fest.getName().toLowerCase().contains(query.toLowerCase()))).forEachOrdered((fest) -> {
            results.add(fest.getName());
        });

        int indexTo = results.size() > MAX_AUTOCOMPLETE_SUGGESTIONS ? MAX_AUTOCOMPLETE_SUGGESTIONS : results.size();
        return results.subList(0, indexTo);
    }

    public List<String> completeFestivalLocation(String query) {
        if (query.length() < AUTOCOMPLETE_THRESHOLD) {
            return null;
        }
        List<String> results = new ArrayList<>();
        List<Festival> existingFestivals = model.getExistingFestivals();

        existingFestivals.stream().filter((fest) -> (fest.getPlace().toLowerCase().contains(query.toLowerCase()))).forEachOrdered((fest) -> {
            results.add(fest.getPlace());
        });

        int indexTo = results.size() > MAX_AUTOCOMPLETE_SUGGESTIONS ? MAX_AUTOCOMPLETE_SUGGESTIONS : results.size();
        return results.subList(0, indexTo);
    }

    public List<String> completePerformerName(String query) {
        if (query.length() < AUTOCOMPLETE_THRESHOLD) {
            return null;
        }
        List<String> results = new ArrayList<>();
        List<Performer> existingPerformers = model.getExistingPerformers();

        existingPerformers.stream().filter((fest) -> (fest.getName().toLowerCase().contains(query.toLowerCase()))).forEachOrdered((fest) -> {
            results.add(fest.getName());
        });

        int indexTo = results.size() > MAX_AUTOCOMPLETE_SUGGESTIONS ? MAX_AUTOCOMPLETE_SUGGESTIONS : results.size();
        return results.subList(0, indexTo);
    }

    private boolean validateInputLevelOne() {
        errorMessage = null;
        if (StringUtils.isNullOrEmpty(model.getName()) || StringUtils.isNullOrEmpty(model.getLocation())
                || null == model.getStartDate() || null == model.getEndDate()) {
            errorMessage = "You haven't entered all required fields in Basic info page!";
            return false;
        }

        Calendar cal = Calendar.getInstance();
        Date now = cal.getTime();
        if (now.after(model.getStartDate()) || now.after(model.getEndDate())) {
            errorMessage = "Basic info: Both dates must be after today!";
            return false;
        }

        if (model.getStartDate().after(model.getEndDate())) {
            errorMessage = "Basic info: Start date must be before or equal to end date";
            return false;
        }

        if (model.getPricePerDay() < 0 || model.getPriceWholeFestival() < 0) {
            errorMessage = "Basic info: Prices must not be negative!";
            return false;
        }

        if (model.getMaxTicketsPerUser() <= 0) {
            errorMessage = "Basic info: User must be allowed at least one ticket";
            return false;
        }

        return true;
    }

    private boolean validatePerformanceDates() {
        for (Performance perf : model.getPerformances()) {
            int diff = util.DateUtil.dateDayDiff(model.getStartDate(), perf.getStartDate());
            if (diff < 0) {
                errorMessage = "One of the performances starts before the festival start date!";
                return false;
            }
            diff = util.DateUtil.dateDayDiff(model.getEndDate(), perf.getStartDate());
            if (diff > 0) {
                errorMessage = "One of the performances starts after the festival end date!";
                return false;
            }
        }
        return true;
    }

    public final void resetModelData() {
        model = new FestivalModel();
        activeStep = 0;
    }

    public void addPerformance() {
        errorMessage = null;
        if(null == model.getStartTime() || null == model.getEndTime()){
            errorMessage = "Start and end time are required!";
            return;
        }
        model.saveCurrentPerformance();
    }

    public void removePerformance() {
        model.getPerformances().remove(model.getSelectedPerformance());
        model.setSelectedPerformance(null);
    }

    public void preview(FileUploadEvent event) {
        model.setCurrentFile(event.getFile());
        model.setPendingUpload(event.getFile());
        previewImage();
    }

    public void previewImage() {
        model.saveFilePreviewContents();
    }

    public void addFile() {
        if (model.getPendingUpload() == null) {
            return;
        }
        boolean ok = true;
        UploadedFile file = model.getPendingUpload();
        if (file.getContentType().startsWith("video")) {
            String type = file.getContentType().split("/")[1];
            switch (type) {
                case "mp4":
                case "webm":
                case "ogg":
                    break;
                default:
                    ok = false;
            }

            if (!ok) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "Supported video types: mp4, webm, ogg"));
                context.getExternalContext().getFlash().setKeepMessages(true);
                return;
            }
        } else {
            if (file.getSize() > 16777215) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "Maximal image size iz 16MB"));
                context.getExternalContext().getFlash().setKeepMessages(true);
                return;
            }
        }

        model.getUploadedFiles().add(model.getPendingUpload());
        model.setCurrentFile(null);
        model.setPendingUpload(null);
    }

    public void removeFile() {
        model.getUploadedFiles().remove(model.getCurrentFile());
        model.setCurrentFile(null);
    }

    public void setModel(FestivalModel model) {
        this.model = model;
    }
}
