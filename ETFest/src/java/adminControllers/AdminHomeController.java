/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminControllers;

import entity.Festival;
import entity.Reservation;
import java.io.Serializable;
import java.util.Calendar;
import static java.util.Calendar.YEAR;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.chartistjsf.model.chart.AspectRatio;
import org.chartistjsf.model.chart.BarChartModel;
import org.chartistjsf.model.chart.BarChartSeries;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Petar
 */
public class AdminHomeController implements Serializable {

    private BarChartModel barModelViews;
    private BarChartModel barModelTickets;

    private Map<Integer, Integer> festivalTickets;
    private List<Festival> topViewed;
    private List<Festival> topTickets;

    /**
     * Creates a new instance of AdminHomeController
     */
    public AdminHomeController() {
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            topViewed = session.createCriteria(Festival.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("canceled", false))
                    .addOrder(Order.desc("views"))
                    .list();

            topViewed = (topViewed.size() > 5 ? topViewed.subList(0, 5) : topViewed);

            topTickets = session.createCriteria(Festival.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("canceled", false))
                    .list();
            topTickets = filterByTickets(topTickets, session);

            createBarModelViews(topViewed);
            createModelTickets(topTickets);
        } catch (HibernateException ex) {

        } catch (Exception ex) {

        } finally {
            if (null != tx) {
                tx.commit();
            }
            if (null != session) {
                session.close();
            }
        }
    }

    public BarChartModel getBarModelViews() {
        return barModelViews;
    }

    public BarChartModel getBarModelTickets() {
        return barModelTickets;
    }

    private List<Festival> filterByTickets(List<Festival> topTickets, Session session) {
        festivalTickets = new HashMap<>();
        topTickets.forEach((fest) -> {
            List<Reservation> purchasedReservations = session.createCriteria(Reservation.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("festival", fest))
                    .add(Restrictions.eq("purchased", true))
                    .list();

            int total = 0;
            total = purchasedReservations.stream().map((res) -> res.getNumOfTickets()).reduce(total, Integer::sum);
            festivalTickets.put(fest.getId(), total);
        });

        topTickets.sort((o1, o2) -> {
            Festival fest1 = (Festival) o1;
            Festival fest2 = (Festival) o2;

            return festivalTickets.get(fest2.getId()) - festivalTickets.get(fest1.getId());
        });

        if (topTickets.size() > 5) {
            return topTickets.subList(0, 5);
        } else {
            return topTickets;
        }
    }

    private void createModelTickets(List<Festival> festivals) {
        barModelTickets = initBarModelTickets(festivals);
    }

    private void createBarModelViews(List<Festival> festivals) {
        barModelViews = initBarModelViews(festivals);
    }

    private BarChartModel initBarModelViews(List<Festival> festivals) {
        BarChartModel model = new BarChartModel();
        model.setAspectRatio(AspectRatio.GOLDEN_SECTION);
        model.addLabel("Festival");
        final Calendar cal = Calendar.getInstance();
        festivals.forEach((fest) -> {
            cal.setTime(fest.getStartDate());
            BarChartSeries series = new BarChartSeries();
            series.setName(fest.getName() + " " + cal.get(YEAR));
            series.set(fest.getViews());

            model.addSeries(series);
        });

        model.setShowTooltip(true);
        model.setSeriesBarDistance(30);
        model.setAnimateAdvanced(true);

        return model;
    }

    private BarChartModel initBarModelTickets(List<Festival> festivals) {
        BarChartModel model = new BarChartModel();
        model.setAspectRatio(AspectRatio.GOLDEN_SECTION);
        model.addLabel("Festival");
        final Calendar cal = Calendar.getInstance();
        festivals.forEach((fest) -> {
            cal.setTime(fest.getStartDate());
            BarChartSeries series = new BarChartSeries();
            series.setName(fest.getName() + " " + cal.get(YEAR));
            series.set(festivalTickets.get(fest.getId()));

            model.addSeries(series);
        });

        model.setShowTooltip(true);
        model.setSeriesBarDistance(30);
        model.setAnimateAdvanced(true);

        return model;
    }

    public List<Festival> getTopViewed() {
        return topViewed;
    }

    public List<Festival> getTopTickets() {
        return topTickets;
    }

    public String getYear(Festival fest){
        Calendar cal = Calendar.getInstance();
        cal.setTime(fest.getStartDate());
        return String.valueOf(cal.get(Calendar.YEAR));
    }
    
    public int getTickets(Festival fest){
        return festivalTickets.get(fest.getId());
    }
}
