/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminControllers;

import entity.User;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Petar
 */
@ManagedBean(eager = false, name = "adminLoginController")
@RequestScoped
public class AdminLoginController {

    private String username;
    private String password;
    private String errorMessage;

    /**
     * Creates a new instance of AdminLoginController
     */
    public AdminLoginController() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void login() {
        errorMessage = null;
        if (null == username || null == password || "".equals(username) || "".equals(password)) {
            errorMessage = "Username and password are required";
            return;
        }

        Session session = null;
        Transaction tx = null;
        try {

            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            String passwordMD5 = util.PasswordUtil.getMD5(password);
            Query query = session.createQuery("from User as usr where usr.username= :uname and usr.password='" + passwordMD5 + "' and is_admin=1");
            query.setParameter("uname", username);
            User user = (User) query.uniqueResult();
            if (null == user) {
                throw new NoResultException();
            }
            HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            httpSession.setAttribute("currentUser", user);
            
            FacesContext context = FacesContext.getCurrentInstance();
            context.getApplication().getNavigationHandler().handleNavigation(context, null, "adminHome?faces-redirect=true");
        } catch (NoResultException ex) {
            errorMessage = "Invalid login parameters";
        } catch (HibernateException ex) {
            errorMessage = "An error occured. Please, try again";
        } catch (Exception ex) {
            errorMessage = "An error occured. Please, try again";
        } finally {
            if (null != tx) {
                tx.commit();
            }
            if (null != session) {
                session.close();
            }
        }

    }
}
