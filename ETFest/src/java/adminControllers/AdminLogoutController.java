/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminControllers;

import javax.faces.context.FacesContext;

/**
 *
 * @author Petar
 */
public class AdminLogoutController {

    /**
     * Creates a new instance of AdminLogoutController
     */
    public AdminLogoutController() {
    }
    
    public String logout(){
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "adminLogin?faces-redirect=true";
    }
}
