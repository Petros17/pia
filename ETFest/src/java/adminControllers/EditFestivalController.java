/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminControllers;

import com.mysql.jdbc.StringUtils;
import dataModels.FestivalModel;
import entity.Festival;
import entity.File;
import entity.Message;
import entity.Performance;
import entity.Performer;
import entity.Reservation;
import entity.SocialLink;
import entity.UserMessage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import static javax.faces.application.FacesMessage.SEVERITY_FATAL;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Petar
 */
public class EditFestivalController {

    private static final int AUTOCOMPLETE_THRESHOLD = 2;
    private static final int MAX_AUTOCOMPLETE_SUGGESTIONS = 8;

    private Festival festival;
    private FestivalModel model;
    private String errorMessage;
    private String cancelationReason;

    final Map<String, Boolean> userNotified = new HashMap<>();

    private List<Performance> removedPerformances;

    /**
     * Creates a new instance of EditFestivalController
     */
    public EditFestivalController() {
        FacesContext context = FacesContext.getCurrentInstance();
        festival = (Festival) ((HttpSession) context.getExternalContext().getSession(true)).getAttribute("currentFestival");
        if (null == festival) {
            context.addMessage(null, new FacesMessage(SEVERITY_FATAL, "Error", "Error in accessing festival details!"));
            context.getExternalContext().getFlash().setKeepMessages(true);
            context.getApplication().getNavigationHandler().handleNavigation(context, null, "adminHome?faces-redirect=true");
            return;
        }

        model = new FestivalModel(festival);
        removedPerformances = new ArrayList<>();
    }

    public void cancelFestival() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);
        if (festival.isCanceled()) {
            context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "The festival is canceled, no editing possible"));
            return;
        }

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            festival = (Festival) session.get(Festival.class, festival.getId());

            String message = festival.getName() + " was canceled. Reason: " + cancelationReason + ". If you have already purchased a ticket, your money will be returned.";
            notifyUsers(session, message);

            festival.setCanceled(true);
            session.update(festival);

            session.flush();
            tx.commit();
            userNotified.clear();
            context.addMessage(null, new FacesMessage("Success", "Festival successfully canceled"));
            context.getExternalContext().getFlash().setKeepMessages(true);
            context.getApplication().getNavigationHandler().handleNavigation(context, null, "adminHome?faces-redirect=true");
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again"));
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again"));
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    private void deleteAvailableTickets(Session session) {
        festival.getAvailableTicketses().forEach(session::delete);
    }

    private void deletePerformances(Session session) {
        festival.getPerformances().forEach(session::delete);
    }

    public void saveBasicInfo() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);
        if (festival.isCanceled()) {
            context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "The festival is canceled, no editing possible"));
            return;
        }

        if (!validateInputLevelOne()) {
            return;
        }

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            boolean notifyUsers = updateFestivalBasicInfo(session);
            if (notifyUsers) {
                notifyUsers(session, "There has been a change for festival " + festival.getName() + ". Review the change on festival page.");
            }

            tx.commit();
            session.flush();

            session.refresh(festival);
            model.setFestival(festival);
            model.reset();
            context.addMessage(null, new FacesMessage("Success", "Festival successfully edited"));
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong. Please, try again."));
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong. Please, try again."));
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    public void saveSchedule() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);
        if (festival.isCanceled()) {
            context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "The festival is canceled, no editing possible"));
            return;
        }
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            final Session tmpSession = session;

            //festival = (Festival) session.get(Festival.class, festival.getId());
            removedPerformances.forEach((perf) -> {
                try {
                    tmpSession.delete(perf);
                    festival.getPerformances().remove(perf);
                } catch (HibernateException ex) {

                }
            });

            model.getPerformances().forEach((perf) -> {
                try {
                    tmpSession.update(perf);
                } catch (HibernateException ex) {
                    tmpSession.saveOrUpdate(perf.getPerformer());
                    perf.setFestival(festival);
                    festival.getPerformances().add(perf);
                    perf.getPerformer().getPerformances().add(perf);
                    tmpSession.persist(perf);
                }
            });

            String message = "There are changes in " + festival.getName() + " schedule. Go to festival page to view details.";

            notifyUsers(session, message);

            tx.commit();
            session.flush();
            session.refresh(festival);
            model.setFestival(festival);
            model.reset();
            removedPerformances = new ArrayList<>();

            context.addMessage(null, new FacesMessage("Success", "Schedule successfully edited"));
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again"));
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again"));
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    public void reset() {
        errorMessage = null;
        model.reset();
        userNotified.clear();
    }

    public void editPerformance() {
        Performance perf = model.getSelectedPerformance();
        if (null == perf) {
            return;
        }

        model.setPerformerName(perf.getPerformer().getName());
        model.setPerformanceFestDay(util.DateUtil.dateDayDiff(festival.getStartDate(), perf.getStartDate()) + 1);
        model.setStartTime(perf.getStartTime());
        model.setEndTime(perf.getEndTime());
    }

    public void updatePerformance() {
        model.updateCurrentPerformance();
    }

    public void deletePerformance() {
        Performance perf = model.getSelectedPerformance();
        if (null == perf) {
            return;
        }
        model.getPerformances().remove(perf);
        removedPerformances.add(perf);
    }

    public void addPerformance() {
        errorMessage = null;
        if (null == model.getStartTime() || null == model.getEndTime()) {
            errorMessage = "Start and end time are required";
            return;
        }
        model.saveCurrentPerformance();
    }

    private boolean validateInputLevelOne() {
        errorMessage = null;
        if (StringUtils.isNullOrEmpty(model.getName()) || StringUtils.isNullOrEmpty(model.getLocation())
                || null == model.getStartDate() || null == model.getEndDate()) {
            errorMessage = "You haven't entered all required fields";
            return false;
        }

        Calendar cal = Calendar.getInstance();
        Date now = cal.getTime();
        if (festival.getStartDate().before(now) && !model.getStartDate().equals(festival.getStartDate())) {
            errorMessage = "Festival already started. Start date cannot be changed";
            return false;
        }

        if (now.after(model.getEndDate())) {
            errorMessage = "End date must be after today!";
            return false;
        }

        if (model.getStartDate().after(model.getEndDate())) {
            errorMessage = "Start date must be before or equal to end date";
            return false;
        }

        if (model.getPricePerDay() < 0 || model.getPriceWholeFestival() < 0) {
            errorMessage = "Prices must not be negative!";
            return false;
        }

        if (model.getMaxTicketsPerUser() <= 0) {
            errorMessage = "User must be allowed at least one ticket";
            return false;
        }

        if (model.getNumTicketsPerDay() < model.getSoldTickets()) {
            errorMessage = "Number of tickets per day is less than number of already sold tickets";
            return false;
        }

        return true;
    }

    private boolean updateFestivalBasicInfo(Session session) {
        boolean notifyUsers = false;

        festival.setName(model.getName());
        notifyUsers = !festival.getPlace().equals(model.getLocation());
        festival.setPlace(model.getLocation());
        notifyUsers = notifyUsers || festival.getPricePerDay() != model.getPricePerDay();
        festival.setPricePerDay(model.getPricePerDay());
        notifyUsers = notifyUsers || festival.getPriceWholeFest() != model.getPriceWholeFestival();
        festival.setPriceWholeFest(model.getPriceWholeFestival());

        festival.setMaxTicketsPerUser(model.getMaxTicketsPerUser());

        festival.getAvailableTicketses().forEach((tick) -> {
            int soldTickets = tick.getInitialTickets() - tick.getTicketsLeft();
            tick.setInitialTickets(model.getNumTicketsPerDay());
            tick.setTicketsLeft(model.getNumTicketsPerDay() - soldTickets);
            session.update(tick);
        });

        List<SocialLink> socialLinks = new ArrayList<>(festival.getSocialLinks());
        Set<SocialLink> socialLinksSet = new HashSet<>(0);
        Map<String, String> socialMediaLinks = model.getSocialMediaLinks();
        socialMediaLinks.forEach((t, u) -> {
            boolean exists = false;
            for (SocialLink link : socialLinks) {
                if (link.getNetwork().equals(t)) {
                    if (StringUtils.isNullOrEmpty(u)) {
                        session.delete(link);
                    } else {
                        link.setLink(u);
                        session.update(link);
                        socialLinksSet.add(link);
                    }

                    exists = true;
                    break;
                }
            }
            if (!exists) {
                SocialLink socialLink = new SocialLink(festival, t, u);
                socialLinksSet.add(socialLink);
                session.persist(socialLink);
            }
        });
        
        festival.setSocialLinks(socialLinksSet);
        session.update(festival);
        return notifyUsers;
    }

    private void notifyUsers(Session session, String content) {
        Message message = new Message();
        message.setContent(content);
        message.setFestival(festival);
        message.setDate(Calendar.getInstance().getTime());
        session.persist(message);
        Hibernate.initialize(message.getUserMessages());

        session.refresh(festival);
        Hibernate.initialize(festival.getMessages());
        festival.getMessages().add(message);
        Hibernate.initialize(festival.getReservations());
        
        List<Reservation> reservations = new ArrayList<>(festival.getReservations());
        reservations = reservations.stream().filter((res) -> !res.isExpired() && res.getUser() != null).collect(Collectors.toList());

        reservations.forEach((res) -> {
            if (userNotified.get(res.getUser().getUsername()) == null) {
                UserMessage um = new UserMessage(message, res.getUser(), false);
                message.getUserMessages().add(um);
                res.getUser().getUserMessages().add(um);
                session.persist(um);
                userNotified.put(res.getUser().getUsername(), true);
            }
        });
    }

    public void preview(FileUploadEvent event) {
        model.setCurrentFile(event.getFile());
        model.setPendingUpload(event.getFile());
        previewImage();
    }

    public void previewImage() {
        model.saveFilePreviewContents();
    }

    public void addFile() {
        if (model.getPendingUpload() == null) {
            return;
        }
        boolean ok = true;
        UploadedFile file = model.getPendingUpload();

        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);
        if (file.getContentType().startsWith("video")) {
            String type = file.getContentType().split("/")[1];
            switch (type) {
                case "mp4":
                case "webm":
                case "ogg":
                    break;
                default:
                    ok = false;
            }

            if (!ok) {
                context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "Supported video types: mp4, webm, ogg"));
                return;
            }
        } else {
            if (file.getSize() > 16777215) {
                context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "Maximal image size iz 16MB"));
                return;
            }
        }

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            util.FileUtil.persistFile(session, model.getFestival(), file, null);
            List<File> allFiles = session.createCriteria(File.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("festivalName", festival.getName()))
                    .add(Restrictions.eq("isApproved", true))
                    .list();

            model.setAllFiles(allFiles);

            tx.commit();
            session.flush();
            context.addMessage(null, new FacesMessage("Success", "File successfully added!"));
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong. Please, try again"));
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong. Please, try again"));
        } finally {
            if (null != session) {
                session.close();
            }
        }

    }

    public void removeFile() {
        File file = model.getCurrentFileToPreview();
        if (null == file) {
            return;
        }
        Session session = null;
        Transaction tx = null;
        FacesContext context = FacesContext.getCurrentInstance();

        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            if (file.getIsVideo()) {
                util.FileUtil.deleteVideo(model.getVideoToPreview());
            }
            session.delete(file);

            tx.commit();
            session.flush();

            model.getAllFiles().remove(file);
            model.setCurrentFileToPreview(null);
            context.addMessage(null, new FacesMessage("Success!", "You have successfully removed the file"));
        } catch (HibernateException | IOException ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error!", "Something went wrong, please try again"));
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    public List<String> completePerformerName(String query) {
        if (query.length() < AUTOCOMPLETE_THRESHOLD) {
            return null;
        }
        List<String> results = new ArrayList<>();
        List<Performer> existingPerformers = model.getExistingPerformers();

        existingPerformers.stream().filter((fest) -> (fest.getName().toLowerCase().contains(query.toLowerCase()))).forEachOrdered((fest) -> {
            results.add(fest.getName());
        });

        int indexTo = results.size() > MAX_AUTOCOMPLETE_SUGGESTIONS ? MAX_AUTOCOMPLETE_SUGGESTIONS : results.size();
        return results.subList(0, indexTo);
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public FestivalModel getModel() {
        return model;
    }

    public void setModel(FestivalModel model) {
        this.model = model;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getCancelationReason() {
        return cancelationReason;
    }

    public void setCancelationReason(String cancelationReason) {
        this.cancelationReason = cancelationReason;
    }

    public StreamedContent getImage() {
        if (null == model.getCurrentFileToPreview()) {
            return new DefaultStreamedContent();
        }
        return new DefaultStreamedContent(new ByteArrayInputStream(model.getCurrentFileToPreview().getContent()));
    }
}
