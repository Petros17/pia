/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminControllers;

import dataModels.FestivalModel;
import dataModels.FestivalScheduleModel;
import entity.SocialLink;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import util.CSVParser;
import util.FileParseException;
import util.JsonParser;

/**
 *
 * @author Petar
 */
@Named
@SessionScoped
public class ImportFestivalController implements Serializable {

    private UploadedFile currentFile;
    private FestivalModel model;
    private FestivalScheduleModel previewScheduleModel;

    @ManagedProperty(value = "#{addFestivalController}")
    private AddFestivalController addFestivalController;

    /**
     * Creates a new instance of ImportFestivalController
     */
    public ImportFestivalController() {
    }

    public void upload(FileUploadEvent event) {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);

        this.currentFile = event.getFile();

        if (!(currentFile.getFileName().endsWith("csv") || currentFile.getFileName().endsWith("json"))) {
            context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "Only .csv and .json files allowed"));
            return;
        }
        try {
            if (currentFile.getFileName().endsWith("csv")) {
                model = new CSVParser(currentFile).parse();
            } else {
                model = new JsonParser(currentFile).parse();
            }
            context.getApplication().getNavigationHandler().handleNavigation(context, null, "importedFestivalGallery?faces-redirect=true");
        } catch (FileParseException ex) {
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", ex.getMessage()));
        }
    }

    public void filePreview(FileUploadEvent event) {
        model.setCurrentFile(event.getFile());
        model.setPendingUpload(event.getFile());
        model.saveFilePreviewContents();
    }

    public void previewImage() {
        model.saveFilePreviewContents();
    }

    public void addFile() {
        if (model.getPendingUpload() == null) {
            return;
        }
        boolean ok = true;
        UploadedFile file = model.getPendingUpload();
        if (file.getContentType().startsWith("video")) {
            String type = file.getContentType().split("/")[1];
            switch (type) {
                case "mp4":
                case "webm":
                case "ogg":
                    break;
                default:
                    ok = false;
            }

            if (!ok) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "Supported video types: mp4, webm, ogg"));
                context.getExternalContext().getFlash().setKeepMessages(true);
                return;
            }
        } else {
            if (file.getSize() > 16777215) {
                FacesContext context = FacesContext.getCurrentInstance();
                context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "Maximal image size iz 16MB"));
                context.getExternalContext().getFlash().setKeepMessages(true);
                return;
            }
        }

        model.getUploadedFiles().add(model.getPendingUpload());
        model.setCurrentFile(null);
        model.setPendingUpload(null);
    }

    public void removeFile() {
        model.getUploadedFiles().remove(model.getCurrentFile());
        model.setCurrentFile(null);
    }

    public void preview() {
        addFestivalController.setErrorMessage("");
        previewScheduleModel = new FestivalScheduleModel(model.getPerformances(), model.getStartDate(), model.getEndDate());
        FacesContext context = FacesContext.getCurrentInstance();
        context.getApplication().getNavigationHandler().handleNavigation(context, null, "preview?faces-redirect=true");
    }

    public String viewImageContents(UploadedFile file) {
        return Base64.getEncoder().encodeToString(file.getContents());
    }

    public void save() {
        addFestivalController.setModel(model);
        addFestivalController.save();
    }

    public UploadedFile getCurrentFile() {
        return currentFile;
    }

    public void setCurrentFile(UploadedFile currentFile) {
        this.currentFile = currentFile;
    }

    public AddFestivalController getAddFestivalController() {
        return addFestivalController;
    }

    public void setAddFestivalController(AddFestivalController addFestivalController) {
        this.addFestivalController = addFestivalController;
    }

    public FestivalModel getModel() {
        return model;
    }

    public void setModel(FestivalModel model) {
        this.model = model;
    }

    public FestivalScheduleModel getPreviewScheduleModel() {
        return previewScheduleModel;
    }

    public void setPreviewScheduleModel(FestivalScheduleModel previewScheduleModel) {
        this.previewScheduleModel = previewScheduleModel;
    }

    public List<SocialLink> getSocialLinks() {
        List<SocialLink> links = new ArrayList<>();
        model.getSocialMediaLinks().forEach((t, u) -> {
            links.add(new SocialLink(null, t, u));
        });
        return links;
    }

    public void setDummy(String dummy) {

    }

    public String getDummy() {
        return String.valueOf(1);
    }

    public List<UploadedFile> getImages() {
        return model.getUploadedFiles().stream().filter((f) -> f.getContentType().startsWith("image")).collect(Collectors.toList());
    }
}
