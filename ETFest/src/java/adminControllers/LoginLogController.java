/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminControllers;

import entity.User;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Petar
 */
public class LoginLogController {

    private List<User> mostRecentLogins;
    
    /**
     * Creates a new instance of LoginLogController
     */
    public LoginLogController() {
        Session session = null;
        Transaction tx = null;
        try{
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            
            mostRecentLogins = session.createCriteria(User.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.ne("isAdmin", true))
                    .add(Restrictions.isNotNull("lastLogin"))
                    .addOrder(Order.desc("lastLogin"))
                    .setMaxResults(10).list();
        } catch(HibernateException ex){
            mostRecentLogins = new ArrayList<>();
        } finally {
            if(null != tx){
                tx.commit();
            }
            if(null != session){
                session.close();
            }
        }
    }

    public List<User> getMostRecentLogins() {
        return mostRecentLogins;
    }
}
