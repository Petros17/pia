/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminControllers;

import entity.RegistrationRequest;
import entity.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Petar
 */
public class RegistrationRequestController implements Serializable {

    private List<RegistrationRequest> requests;
    private RegistrationRequest selected;

    /**
     * Creates a new instance of RegistrationRequestListener
     */
    public RegistrationRequestController() {
        update();
    }

    public final void update() {
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            requests = session.createCriteria(RegistrationRequest.class).list();
            return;
        } catch (HibernateException ex) {

        } catch (Exception ex) {

        } finally {
            if (null != tx) {
                tx.commit();
            }
            if (null != session) {
                session.close();
            }
        }
        requests = new ArrayList<>();
    }

    public int getNumberOfRequests() {
        return requests.size();
    }

    public List<RegistrationRequest> getRequests() {
        update();
        return requests;
    }

    public RegistrationRequest getSelected() {
        return selected;
    }

    public void setSelected(RegistrationRequest selected) {
        this.selected = selected;
    }

    public void approve() {
        if (null == selected) {
            return;
        }

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            session.delete(selected);
            requests.remove(selected);

            User user = new User(selected.getUsername(),
                    selected.getPassword(),
                    selected.getEmail(),
                    selected.getPhone(),
                    selected.getFirstName(),
                    selected.getLastName(),
                    false, /*is_admin*/
                    false, /*is_blocked*/
                    0, /*missed_reservations*/
                    false /*social_account*/);
            user.setIsBlocked(false);
            session.persist(user);

            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(
                    null, new FacesMessage("Success!", "You have successfully approved request for " + selected.getUsername()));
            context.getExternalContext()
                    .getFlash().setKeepMessages(true);
            
            selected = null;
            tx.commit();
            session.flush();
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    public void reject() {
        if (null == selected) {
            return;
        }

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            session.delete(selected);
            requests.remove(selected);
            
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(
                    null, new FacesMessage("Success!", "You have successfully rejected request for " + selected.getUsername()));
            context.getExternalContext()
                    .getFlash().setKeepMessages(true);
            
            selected = null;
            
            tx.commit();
            session.flush();
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }
}
