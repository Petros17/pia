/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminControllers;

import entity.AvailableTickets;
import entity.Festival;
import entity.Reservation;
import entity.User;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import javax.faces.context.FacesContext;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import static userControllers.ReservationController.WHOLE_FEST_TICKET;

/**
 *
 * @author Petar
 */
public class TicketsController {

    private static final int SINGLE_DAY = 1;
    private static final int WHOLE_FEST = 2;

    private List<Reservation> reservations;
    private List<Festival> allFestivals;

    private Reservation selected;

    private String selectedFestivalName;
    private Festival selectedFestival;
    private int ticketType;
    private int day = 1;
    private int amount = 1;

    private String festName;
    private String username;

    /**
     * Creates a new instance of TicketsController
     */
    public TicketsController() {
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            updateExpiredReservations(session);

            Date now = Calendar.getInstance().getTime();
            allFestivals = session.createCriteria(Festival.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("canceled", false))
                    .addOrder(Order.asc("name"))
                    .list();
            allFestivals = allFestivals.stream().filter((fest) -> fest.getEndDate().after(now)).collect(Collectors.toList());

            selectedFestival = allFestivals.get(0);
            tx.commit();
            session.flush();
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    private void updateExpiredReservations(Session session) throws HibernateException {
        List<Reservation> activeReservations = session.createCriteria(Reservation.class)
                .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                .add(Restrictions.ne("expired", true))
                .add(Restrictions.ne("purchased", true))
                .list();

        Date now = Calendar.getInstance().getTime();
        activeReservations.forEach((res) -> {
            if (util.DateUtil.dateHourDiff(res.getDate(), now) > 48) {
                res.setExpired(true);
                releaseFestTickets(res, session);
                User user = res.getUser();
                user.setMissedReservations(user.getMissedReservations() + 1);
                session.update(res);
                if (user.getMissedReservations() > 2) {
                    user.setIsBlocked(true);
                }
                session.update(user);
            }
        });

        reservations = activeReservations.stream().filter((res) -> !res.isExpired()).collect(Collectors.toList());
    }

    private void releaseFestTickets(Reservation res, Session session) {
        res.getFestival().getAvailableTicketses().forEach((elem) -> {
            if (res.isForWholeFestival() || (elem.getDay() == res.getDay())) {
                elem.setTicketsLeft(elem.getTicketsLeft() + res.getNumOfTickets());
                session.update(elem);
            }
        });
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public Reservation getSelected() {
        return selected;
    }

    public void setSelected(Reservation selected) {
        this.selected = selected;
    }

    public List<Festival> getAllFestivals() {
        return allFestivals;
    }

    public void setAllFestivals(List<Festival> allFestivals) {
        this.allFestivals = allFestivals;
    }

    public String getSelectedFestivalName() {
        return selectedFestivalName;
    }

    public void setSelectedFestivalName(String selectedFestivalName) {
        this.selectedFestivalName = selectedFestivalName;
        selectedFestival = allFestivals.stream().filter((fest) -> fest.getName().equals(selectedFestivalName)).findFirst().get();
    }

    public Festival getSelectedFestival() {
        return selectedFestival;
    }

    public void setSelectedFestival(Festival selectedFestival) {
        this.selectedFestival = selectedFestival;
    }

    public int getTicketType() {
        return ticketType;
    }

    public void setTicketType(int ticketType) {
        this.ticketType = ticketType;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void confirmReservation() {
        if (null == selected) {
            return;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            selected.setPurchased(true);
            session.update(selected);

            reservations.remove(selected);
            tx.commit();
            session.flush();
            context.addMessage(null, new FacesMessage("Success"));
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "There has been an error. Please, try again"));
        } finally {
            if (null != session) {
                session.close();
            }
        }
        context.getExternalContext().getFlash().setKeepMessages(true);
    }

    public void sellTickets() {
        if (0 == amount || null == selectedFestival) {
            return;
        }
        FacesContext context = FacesContext.getCurrentInstance();

        if (!canSell()) {
            context.getExternalContext().getFlash().setKeepMessages(true);
            return;
        }

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            Reservation reservation = new Reservation(selectedFestival, WHOLE_FEST == ticketType, amount, true, Calendar.getInstance().getTime(), false);
            reservation.setDay(day);
            session.refresh(selectedFestival);
            Hibernate.initialize(selectedFestival.getReservations());
            selectedFestival.getReservations().add(reservation);

            session.persist(reservation);
            updateAvailableTickets(session);

            session.update(selectedFestival);

            tx.commit();
            session.flush();

            context.addMessage(null, new FacesMessage("Success", "Tickets successfully sold!"));
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again"));
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    public String getFestName() {
        return festName;
    }

    public void setFestName(String festName) {
        this.festName = festName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void search() {
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            reservations = session.createCriteria(Reservation.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.ne("expired", true))
                    .add(Restrictions.ne("purchased", true))
                    .list();
            reservations = reservations.stream()
                    .filter((res) -> res.getUser().getDisplayName().toLowerCase().contains(username.toLowerCase()))
                    .filter((res) -> res.getFestival().getName().toLowerCase().contains(festName.toLowerCase()))
                    .collect(Collectors.toList());
        } catch (HibernateException ex) {

        } finally {
            if (null != tx) {
                tx.commit();
            }
            if (null != session) {
                session.close();
            }
        }
    }

    public String description(Reservation res) {
        if (res.isForWholeFestival()) {
            return "Whole fest";
        } else {
            return "Day " + res.getDay();
        }
    }

    public List<Integer> getFestDays() {
        List<Integer> result = new ArrayList<>();
        if (null == selectedFestival) {
            result.add(1);
        } else {
            int dayDiff = util.DateUtil.dateDayDiff(selectedFestival.getStartDate(), selectedFestival.getEndDate()) + 1;
            for (int i = 1; i <= dayDiff; i++) {
                result.add(i);
            }
        }
        return result;
    }

    public int ticketsLeft() {
        if (null == selectedFestival) {
            return 0;
        }

        if (SINGLE_DAY == ticketType) {
            AvailableTickets tick = selectedFestival.getAvailableTicketses().stream().filter((elem) -> elem.getDay() == day).findFirst().get();
            return tick.getTicketsLeft();
        } else {
            int min = Integer.MAX_VALUE;
            for (AvailableTickets tick : selectedFestival.getAvailableTicketses()) {
                if (tick.getTicketsLeft() < min) {
                    min = tick.getTicketsLeft();
                }
            }
            return min;
        }
    }

    private boolean canSell() {
        FacesContext context = FacesContext.getCurrentInstance();

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            if (WHOLE_FEST_TICKET == ticketType) {
                for (AvailableTickets elem : selectedFestival.getAvailableTicketses()) {
                    if (elem.getTicketsLeft() < amount) {
                        context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "There are not enough tickets for day " + elem.getDay()));
                        return false;
                    }
                }
            } else {
                AvailableTickets tick = selectedFestival.getAvailableTicketses().stream().filter((elem) -> elem.getDay() == day).findFirst().get();
                if (tick.getTicketsLeft() < amount) {
                    context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "There are not enough tickets left"));
                    return false;
                }
            }

            return true;
        } catch (HibernateException ex) {
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again!"));
            return false;
        } catch (Exception ex) {
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again!"));
            return false;
        } finally {
            if (null != tx) {
                tx.commit();
            }
            if (null != session) {
                session.close();
            }
        }
    }

    private void updateAvailableTickets(Session session) {
        if (WHOLE_FEST == ticketType) {
            selectedFestival.getAvailableTicketses().forEach((elem) -> {
                elem.setTicketsLeft(elem.getTicketsLeft() - amount);
                session.update(elem);
            });
        } else {
            AvailableTickets elem = selectedFestival.getAvailableTicketses().stream().filter((tick) -> tick.getDay() == day).findFirst().get();
            elem.setTicketsLeft(elem.getTicketsLeft() - amount);
            session.update(elem);
        }
    }
}
