/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package adminControllers;

import dataModels.Video;
import entity.File;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import javax.faces.context.FacesContext;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Petar
 */
public class UploadRequestController implements Serializable {

    private List<File> requests;
    private File selected;
    private Video selectedVideo;

    /**
     * Creates a new instance of UploadRequestController
     */
    public UploadRequestController() {
        update();
    }

    public List<File> getRequests() {
        return requests;
    }

    public void setRequests(List<File> requests) {
        this.requests = requests;
    }

    public File getSelected() {
        return selected;
    }

    public void setSelected(File selected) {
        this.selected = selected;
        if (null != selected) {
            selectedVideo = new Video(selected.getFestivalName(), selected.getName());
        }
    }

    public Video getSelectedVideo() {
        return selectedVideo;
    }

    public void setSelectedVideo(Video selectedVideo) {
        this.selectedVideo = selectedVideo;
    }

    public StreamedContent getImage() {
        if (null == selected) {
            return new DefaultStreamedContent();
        }
        return new DefaultStreamedContent(new ByteArrayInputStream(selected.getContent()));
    }

    public void approve() {
        if (null == selected) {
            return;
        }

        Session session = null;
        Transaction tx = null;
        FacesContext context = FacesContext.getCurrentInstance();

        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            selected.setIsApproved(true);
            session.update(selected);

            tx.commit();
            session.flush();

            requests.remove(selected);
            selected = null;
            selectedVideo = null;
            context.addMessage(null, new FacesMessage("Success!", "You have successfully approved the file upload"));
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error!", "Something went wrong, please try again"));
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    public void reject() {
        if (null == selected) {
            return;
        }

        Session session = null;
        Transaction tx = null;
        FacesContext context = FacesContext.getCurrentInstance();

        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            if (selected.getIsVideo()) {
                util.FileUtil.deleteVideo(selectedVideo);
            }
            session.delete(selected);

            tx.commit();
            session.flush();

            requests.remove(selected);
            selected = null;
            selectedVideo = null;
            context.addMessage(null, new FacesMessage("Success!", "You have successfully rejected the file upload"));
        } catch (HibernateException | IOException ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error!", "Something went wrong, please try again"));
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    public final void update(){
        requests = new ArrayList<>();
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            requests = session.createCriteria(File.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("isApproved", false))
                    .list();
            
            requests.forEach((req)-> {
                Hibernate.initialize(req.getUser());
            } );
        } catch (HibernateException ex) {

        } catch (Exception ex) {

        } finally {
            if (null != tx) {
                tx.commit();
            }
            if (null != session) {
                session.close();
            }
        }
    }
    
    public int getNumberOfRequests() {
        return requests.size();
    }
}
