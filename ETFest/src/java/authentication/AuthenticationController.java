/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package authentication;

import entity.User;
import java.io.Serializable;
import javax.faces.application.ConfigurableNavigationHandler;
import javax.faces.context.FacesContext;
import javax.faces.event.ComponentSystemEvent;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Petar
 */
public class AuthenticationController implements Serializable {

    /**
     * Creates a new instance of AuthenticationController
     */
    public AuthenticationController() {
    }
    
    public void authenticateAdmin(ComponentSystemEvent event){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        User user = (User) session.getAttribute("currentUser");
        if(null == user || !user.isIsAdmin()){
            ConfigurableNavigationHandler nav = (ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            nav.performNavigation("adminLogin?faces-redirect=true");
        }
    }
    
    public void authenticateUser(ComponentSystemEvent event){
        String viewId = FacesContext.getCurrentInstance().getViewRoot().getViewId();
        if(viewId.startsWith("/index") || viewId.startsWith("/userSearch")){
            return;
        }
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        User user = (User) session.getAttribute("currentUser");
        if(null == user || user.isIsAdmin()){
            ConfigurableNavigationHandler nav = (ConfigurableNavigationHandler) FacesContext.getCurrentInstance().getApplication().getNavigationHandler();
            nav.performNavigation("index?faces-redirect=true");
        }
    }
    
    public boolean isAdmin(){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        User user = (User) session.getAttribute("currentUser");
        return (null != user && user.isIsAdmin());
    }
    
    public boolean isUser(){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        User user = (User) session.getAttribute("currentUser");
        return (null != user && !user.isIsAdmin());
    }
    
    public boolean isBlocked(){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        User user = (User) session.getAttribute("currentUser");
        return (null != user && user.isIsBlocked());
    }
}
