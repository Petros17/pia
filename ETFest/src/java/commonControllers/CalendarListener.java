/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonControllers;

import entity.User;
import java.util.Date;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.primefaces.event.SelectEvent;

/**
 *
 * @author Petar
 */
public class CalendarListener {

    /**
     * Creates a new instance of CalendarListener
     */
    public CalendarListener() {
    }
    
    public void calendarChangeListener(SelectEvent event){
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("searchDate", (Date) event.getObject());
        
        User user = (User) session.getAttribute("currentUser");
        if(null==user || !user.isIsAdmin()){
            context.getApplication().getNavigationHandler().handleNavigation(context, null, "userSearch?faces-redirect=true");
        } else if(null != user && user.isIsAdmin()){
            context.getApplication().getNavigationHandler().handleNavigation(context, null, "adminSearch?faces-redirect=true");
        }
        
    }
    
}
