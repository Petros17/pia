/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonControllers;

import com.mysql.jdbc.StringUtils;
import entity.User;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Petar
 */
public class ChangePasswordController {

    private User user;
    private String errorMessage;
    private String currentPassword;
    private String newPassword;
    private String confirmPassword;

    /**
     * Creates a new instance of ChangePasswordController
     */
    public ChangePasswordController() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        this.user = (User) session.getAttribute("currentUser");
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public void change(boolean isAdmin) {
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.addCallbackParam("changeSuccessfull", false);
        if (StringUtils.isNullOrEmpty(currentPassword) || StringUtils.isNullOrEmpty(newPassword) || StringUtils.isNullOrEmpty(confirmPassword)) {
            errorMessage = "All fields are required";
            return;
        }

        if (!user.getPassword().equals(util.PasswordUtil.getMD5(currentPassword))) {
            errorMessage = "Current password is not valid";
            return;
        }

        if (!newPassword.equals(confirmPassword)) {
            errorMessage = "New password and confirmation must match";
            return;
        }

        if (null != (errorMessage = util.PasswordUtil.validatePassword(newPassword))) {
            return;
        }

        FacesContext context = FacesContext.getCurrentInstance();
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            user.setPassword(util.PasswordUtil.getMD5(newPassword));

            session.update(user);
            tx.commit();
            session.flush();

            context.addMessage(
                    null, new FacesMessage("Success!", "You have successfully changed your password"));
            requestContext.addCallbackParam("changeSuccessfull", true);
        } catch (HibernateException ex) {
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "There has been an error, please try again"));

            if (null != tx) {
                tx.rollback();
            }
        } catch (Exception ex) {
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "There has been an error, please try again"));

            if (null != tx) {
                tx.rollback();
            }
        } finally {
            if (null != session) {
                session.close();
            }
        }

        context.getExternalContext()
                .getFlash().setKeepMessages(true);
        context.getApplication()
                .getNavigationHandler().handleNavigation(context, null, (isAdmin ? "adminHome" : "index") + "?faces-redirect=true");
    }

}
