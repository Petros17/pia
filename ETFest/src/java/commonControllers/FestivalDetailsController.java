/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonControllers;

import dataModels.FestivalScheduleModel;
import dataModels.Video;
import entity.Festival;
import entity.File;
import entity.SocialLink;
import entity.User;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Petar
 */
public class FestivalDetailsController {

    private Festival festival;
    private List<File> files;
    private List<Video> videos;

    private FestivalScheduleModel model;

    
    /**
     * Creates a new instance of FestivalDetailsController
     */
    public FestivalDetailsController() {
        videos = new ArrayList<>();
        HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        festival = (Festival) httpSession.getAttribute("currentFestival");

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            files = session.createCriteria(File.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("festivalName", festival.getName()))
                    .add(Restrictions.eq("isApproved", true))
                    .list();

            User user = (User) httpSession.getAttribute("currentUser");
            
            files.forEach((f)->{
                if(f.getIsVideo()){
                   Video video = new Video(festival.getName(), f.getName());
                   videos.add(video);
                }
            });

            files = files.stream().filter((f) -> f.getIsVideo() == null || !f.getIsVideo()).collect(Collectors.toList());
            
            
            session.refresh(festival);
            Hibernate.initialize(festival.getPerformances());
            Hibernate.initialize(festival.getSocialLinks());
            
            if (null == user || !user.isIsAdmin()) {
                festival = (Festival) session.get(Festival.class, festival.getId());
                festival.setViews(festival.getViews() + 1);
                session.update(festival);
            }
            tx.commit();
            session.flush();
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
        } finally {
            if (null != session) {
                session.close();
            }
        }

        model = new FestivalScheduleModel(festival);
    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public List<File> getFiles() {
        return files;
    }

    public void setFiles(List<File> files) {
        this.files = files;
    }

    public List<SocialLink> getSocialLinks() {
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            List<SocialLink> links = new ArrayList<>(festival.getSocialLinks());
            return links;
        } catch (HibernateException ex) {

        } catch (Exception ex) {
            String message = ex.getMessage();
        } finally {
            if (null != tx) {
                tx.commit();
            }
            if (null != session) {
                session.close();
            }
        }
        return null;
    }

    public FestivalScheduleModel getModel() {
        return model;
    }

    public void setModel(FestivalScheduleModel model) {
        this.model = model;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }
}
