/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonControllers;

import entity.File;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import javax.faces.context.FacesContext;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;

/**
 *
 * @author Petar
 */
public class ImageController {

    /**
     * Creates a new instance of ImageController
     */
    public ImageController() {
    }
    
    
    public StreamedContent getImage() throws IOException {
        FacesContext context = FacesContext.getCurrentInstance();
        try {
            String id = context.getExternalContext().getRequestParameterMap().get("fileId");
            int fileId = Integer.parseInt(id);
            Session session = util.HibernateUtil.getSessionFactory().openSession();
            Transaction tx = session.beginTransaction();
            File file = (File) session.get(File.class, fileId);
            tx.commit();
            session.close();
            return new DefaultStreamedContent(new ByteArrayInputStream(file.getContent()));
        } catch (NumberFormatException | HibernateException ex) {

        }
        return new DefaultStreamedContent();
    }
}
