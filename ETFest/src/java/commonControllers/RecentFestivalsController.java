/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonControllers;

import entity.Festival;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Petar
 */
public class RecentFestivalsController implements Serializable {

    private List<Festival> festivals;
    private Map<Integer, Integer> festivalStartDateDiff;
    private Festival selected;
    
    /**
     * Creates a new instance of RecentFestivalsController
     */
    public RecentFestivalsController() {
        festivalStartDateDiff = new HashMap<>();
        Session session = null;
        Transaction tx = null;
        try{
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            
            festivals = session.createCriteria(Festival.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("canceled", false))
                    .list();
            
            Date now = Calendar.getInstance().getTime();
            festivals.removeIf((festival) -> festival.getEndDate().before(now));
            
            festivals.forEach((festival) -> {
                festivalStartDateDiff.put(festival.getId(), util.DateUtil.dateDayDiff(now, festival.getStartDate()));
            });
            
            festivals.sort((o1, o2) -> {
                Festival f1 = (Festival) o1;
                Festival f2 = (Festival) o2;
                
                return festivalStartDateDiff.get(f1.getId()) - festivalStartDateDiff.get(f2.getId());
             });
            
            festivals = (festivals.size() > 5 ? festivals.subList(0, 5) : festivals);
        } catch(HibernateException ex){
            festivals = new ArrayList<>();
        } finally {
            if(null != tx){
                tx.commit();
            }
            if(null != session){
                session.close();
            }
        }
    }

    public List<Festival> getFestivals() {
        return festivals;
    }

    public Festival getSelected() {
        return selected;
    }

    public void setSelected(Festival selected) {
        this.selected = selected;
    }
    
    public void viewFestival(){
        if(null == selected){
            return;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("currentFestival", selected);
        
        context.getApplication().getNavigationHandler().handleNavigation(context, null, "festival?faces-redirect=true");
    }
}
