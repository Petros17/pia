/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonControllers;

import com.mysql.jdbc.StringUtils;
import entity.Festival;
import entity.Performance;
import entity.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Petar
 */
@Named
@ViewScoped
public class SearchFestivalController implements Serializable {

    private String name;
    private Date dateFrom;
    private Date dateTo;
    private String place;
    private String performer;

    private List<Festival> searchResults;
    private Festival selectedFestival;

    /**
     * Creates a new instance of SearchFestivalController
     */
    public SearchFestivalController() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        Date selectDate = (Date) session.getAttribute("searchDate");
        if (null != selectDate) {
            dateFrom = selectDate;
            dateTo = selectDate;
            search();
        }
        session.removeAttribute("searchDate");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Date dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Date getDateTo() {
        return dateTo;
    }

    public void setDateTo(Date dateTo) {
        this.dateTo = dateTo;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getPerformer() {
        return performer;
    }

    public void setPerformer(String performer) {
        this.performer = performer;
    }

    public List<Festival> getSearchResults() {
        return searchResults;
    }

    public void setSearchResults(List<Festival> searchResults) {
        this.searchResults = searchResults;
    }

    public final void search() {
        Date now = Calendar.getInstance().getTime();
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            searchResults = session.createCriteria(Festival.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("canceled", false))
                    .list();
            searchResults.removeIf((festival) -> festival.getEndDate().before(now));
            if (!StringUtils.isNullOrEmpty(name)) {
                searchResults.removeIf((festival) -> !festival.getName().toLowerCase().contains(name.toLowerCase()));
            }
            if (!StringUtils.isNullOrEmpty(place)) {
                searchResults.removeIf((festival) -> !festival.getPlace().toLowerCase().contains(place.toLowerCase()));
            }
            if (null != dateFrom) {
                searchResults.removeIf((festival) -> festival.getEndDate().before(dateFrom));
            }
            if (null != dateTo) {
                searchResults.removeIf((festival) -> festival.getStartDate().after(dateTo));
            }
            
            if (!StringUtils.isNullOrEmpty(performer)) {
                List<Festival> finalResults = new ArrayList<>();
                for (Festival fest : searchResults) {
                    Hibernate.initialize(fest.getPerformances());
                    for (Performance performance : fest.getPerformances()) {
                        if (performance.getPerformer().getName().toLowerCase().contains(performer.toLowerCase())) {
                            finalResults.add(fest);
                            break;
                        }
                    }
                }
                searchResults = finalResults;
            }
        } catch (HibernateException ex) {

        } catch (Exception ex) {

        } finally {
            if (null != tx) {
                tx.commit();
            }
            if (null != session) {
                session.close();
            }
        }
    }

    public Festival getSelectedFestival() {
        return selectedFestival;
    }

    public void setSelectedFestival(Festival selectedFestival) {
        this.selectedFestival = selectedFestival;
    }

    public void view() {
        if (null == selectedFestival) {
            return;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("currentFestival", selectedFestival);

        User user = (User) session.getAttribute("currentUser");
        if (null != user) {
            String redirection = user.isIsAdmin() ? "adminFestival?faces-redirect=true" : "festival?faces-redirect=true";
            context.getApplication().getNavigationHandler().handleNavigation(context, null, redirection);
        } else {
            context.getApplication().getNavigationHandler().handleNavigation(context, null, "index?faces-redirect=true");
        }

    }
}
