/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commonControllers;

import entity.Comment;
import entity.Festival;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.chartistjsf.model.chart.AspectRatio;
import org.chartistjsf.model.chart.BarChartModel;
import org.chartistjsf.model.chart.BarChartSeries;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Petar
 */
public class TopFestivalsController implements Serializable {

    private BarChartModel ratedFestivalsModel;

    private Map<String, Double> festivalRating;
    private List<Festival> topFestivals;

    /**
     * Creates a new instance of TopFestivalsController
     */
    public TopFestivalsController() {
        festivalRating = new HashMap<>();

        Session session = null;
        Transaction tx = null;

        topFestivals = new ArrayList<>();
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            List<Festival> allFestivals = session.createCriteria(Festival.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("canceled", false))
                    .list();

            for (Festival fest : allFestivals) {
                if (festivalRating.get(fest.getName()) != null) {
                    continue;
                }
                List<Comment> comments = session.createCriteria(Comment.class)
                        .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                        .add(Restrictions.eq("festivalName", fest.getName()))
                        .list();

                double rating = 0;
                for (Comment comm : comments) {
                    rating += comm.getRating();
                }
                if (comments.size() > 0) {
                    rating /= comments.size();
                }

                festivalRating.put(fest.getName(), rating);

                if (0 == rating) {
                    continue;
                }
                topFestivals.add(fest);
            }

            topFestivals.sort((o1, o2) -> {
                Festival f1 = (Festival) o1;
                Festival f2 = (Festival) o2;
                double diff = festivalRating.get(f2.getName()) - festivalRating.get(f1.getName());
                return diff > 0 ? 1 : diff < 0 ? -1 : 0;
            });

            topFestivals = (topFestivals.size() > 5 ? topFestivals.subList(0, 5) : topFestivals);
        } catch (HibernateException ex) {

        } catch (Exception ex) {
            String exmesg = ex.getMessage();
            System.out.println(exmesg);
        } finally {
            if (null != tx) {
                tx.commit();
            }
            if (null != session) {
                session.close();
            }
        }

        initBarModel();
    }

    public BarChartModel getRatedFestivalsModel() {
        return ratedFestivalsModel;
    }

    public List<Festival> getTopFestivals() {
        return topFestivals;
    }

    private void initBarModel() {
        ratedFestivalsModel = new BarChartModel();
        ratedFestivalsModel.setAspectRatio(AspectRatio.GOLDEN_SECTION);
        ratedFestivalsModel.addLabel("Festival");
        topFestivals.forEach((fest) -> {
            BarChartSeries series = new BarChartSeries();
            series.setName(fest.getName());
            double value = (double) Math.round(festivalRating.get(fest.getName()) * 100d) / 100d;
            series.set(value);

            ratedFestivalsModel.addSeries(series);
        });

        ratedFestivalsModel.setShowTooltip(true);
        ratedFestivalsModel.setSeriesBarDistance(30);
        ratedFestivalsModel.setAnimateAdvanced(true);
    }

    public double getRating(Festival fest) {
        return festivalRating.get(fest.getName());
    }

}
