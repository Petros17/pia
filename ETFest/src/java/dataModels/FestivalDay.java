/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataModels;

import entity.Performance;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Petar
 */
public class FestivalDay implements Serializable {
    private Date festivalDate;
    private int festivalDay;
    private List<Performance> performances;

    public FestivalDay(Date festivalDate, List<Performance> performances, int festivalDay) {
        this.festivalDate = festivalDate;
        this.performances = performances;
        this.festivalDay = festivalDay;
        
        this.performances.sort((o1, o2) -> {
            Performance p1 = (Performance) o1;
            Performance p2 = (Performance) o2;
            
            return p1.getStartTime().before(p2.getStartTime()) ? -1 : p1.getStartTime().after(p2.getStartTime()) ? 1 : 0;
        });
    }

    public Date getFestivalDate() {
        return festivalDate;
    }

    public void setFestivalDate(Date festivalDate) {
        this.festivalDate = festivalDate;
    }

    public List<Performance> getPerformances() {
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }

    public int getFestivalDay() {
        return festivalDay;
    }

    public void setFestivalDay(int festivalDay) {
        this.festivalDay = festivalDay;
    }
    
    
}
