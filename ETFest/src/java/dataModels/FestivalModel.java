/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataModels;

import entity.AvailableTickets;
import entity.Festival;
import entity.File;
import entity.Performance;
import entity.Performer;
import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.primefaces.model.UploadedFile;
import util.DateUtil;
import util.HibernateUtil;
import util.ImageConverter;

/**
 *
 * @author Petar
 */
public class FestivalModel implements Serializable {

    private String name;
    private String location;
    private Date startDate;
    private Date endDate;
    private int pricePerDay;
    private int priceWholeFestival;
    private int maxTicketsPerUser = 1;
    private int numTicketsPerDay = 1;

    private String performerName;
    private int performanceFestDay;
    private Date startTime;
    private Date endTime;

    private List<Performance> performances;
    private List<Festival> existingFestivals;
    private List<Performer> existingPerformers;

    private final List<SocialMedia> allSocialMedia;
    private List<SocialMedia> selectedSocialMedia;
    private Map<String, String> socialMediaLinks;

    private Performance selectedPerformance;
    private UploadedFile currentFile;
    private UploadedFile pendingUpload;

    private byte[] filePreviewContents;

    private List<UploadedFile> uploadedFiles;

    private List<File> allFiles;
    private Festival festival;
    private int soldTickets;
    private File currentFileToPreview;
    private Video videoToPreview;

    public FestivalModel() {
        performances = new ArrayList<>();
        uploadedFiles = new ArrayList<>();
        allSocialMedia = ImageConverter.social_media;

        selectedSocialMedia = new ArrayList<>();
        socialMediaLinks = new HashMap<>();
    }

    public FestivalModel(Festival festival) {
        this();
        this.festival = festival;

        reset();
    }

    public final void reset() {
        this.name = festival.getName();
        this.location = festival.getPlace();
        this.startDate = festival.getStartDate();
        this.endDate = festival.getEndDate();
        this.pricePerDay = festival.getPricePerDay();
        this.priceWholeFestival = festival.getPriceWholeFest();
        this.maxTicketsPerUser = festival.getMaxTicketsPerUser();
        try {
            this.numTicketsPerDay = festival.getAvailableTicketses().stream().findFirst().get().getInitialTickets();
        } catch (NoSuchElementException ex) {
            this.numTicketsPerDay = 0;
        }

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            session.refresh(festival);
            Hibernate.initialize(festival.getPerformances());
            Hibernate.initialize(festival.getSocialLinks());
            
            this.performances = new ArrayList<>(festival.getPerformances());
            this.performances.sort((o1, o2) -> {
                Performance p1 = (Performance) o1;
                Performance p2 = (Performance) o2;

                return util.DateUtil.dateDayDiff(p2.getStartDate(), p1.getStartDate());
            });
            
            allFiles = session.createCriteria(File.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("festivalName", festival.getName()))
                    .add(Restrictions.eq("isApproved", true))
                    .list();

            soldTickets = calculateSoldTickets();

            (new ArrayList<>(festival.getSocialLinks())).forEach((link) -> {
                ImageConverter.social_media.forEach((sm) -> {
                    if (sm.getName().equals(link.getNetwork())) {
                        selectedSocialMedia.add(sm);
                        socialMediaLinks.put(link.getNetwork(), link.getLink());
                    }
                });
            });
        } catch (HibernateException ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Caught hibernate exception in FestivalModel.reset"));
            context.getExternalContext().getFlash().setKeepMessages(true);
        } catch (Exception ex) {
            FacesContext context = FacesContext.getCurrentInstance();
            context.addMessage(null, new FacesMessage("Caught exception in FestivalModel.reset"));
            context.getExternalContext().getFlash().setKeepMessages(true);
        } finally {
            if (null != tx) {
                tx.commit();
            }
            if (null != session) {
                session.close();
            }
        }
    }

    public int getSoldTickets() {
        return soldTickets;
    }

    private int calculateSoldTickets() {
        int max = 0;
        for (AvailableTickets tick : festival.getAvailableTicketses()) {
            int diff = tick.getInitialTickets() - tick.getTicketsLeft();
            if (diff > max) {
                max = diff;
            }
        }
        return max;

    }

    public Festival getFestival() {
        return festival;
    }

    public void setFestival(Festival festival) {
        this.festival = festival;
    }

    public List<Integer> getDays() {
        List<Integer> result = new ArrayList<>();
        int days = util.DateUtil.dateDayDiff(startDate, endDate) + 1;
        for (int i = 1; i <= days; i++) {
            result.add(i);
        }
        return result;
    }

    public void saveCurrentPerformance() {
        Date tmpEnd = this.endDate;
        Performance performance = new Performance();
        performance.setStartDate(DateUtil.addToDate(startDate, performanceFestDay - 1));
        if (DateUtil.dateHourDiff(startTime, endTime) < 0) {
            endDate = startDate;
        } else {
            endDate = DateUtil.addToDate(startDate, 1);
        }
        performance.setEndDate(endDate);
        performance.setStartTime(startTime);
        performance.setEndTime(endTime);

        Performer performer;
        this.existingPerformers = getExistingPerformers();
        if (!existingPerformers.stream().anyMatch((perf) -> perf.getName().equals(performerName))) {
            performer = new Performer();
            performer.setName(this.performerName);
            existingPerformers.add(performer);
        } else {
            performer = existingPerformers.stream().filter((perf) -> perf.getName().equals(performerName)).findFirst().get();
        }
        performance.setPerformer(performer);
        performances.add(performance);

        performerName = null;
        startTime = null;
        endTime = null;
        this.endDate = tmpEnd;
    }

    public void updateCurrentPerformance() {
        if (null == selectedPerformance) {
            return;
        }
        Date tmpEnd = this.endDate;
        selectedPerformance.setStartDate(DateUtil.addToDate(startDate, performanceFestDay - 1));
        if (DateUtil.dateHourDiff(startTime, endTime) < 0) {
            endDate = startDate;
        } else {
            endDate = DateUtil.addToDate(startDate, 1);
        }
        selectedPerformance.setEndDate(endDate);
        selectedPerformance.setStartTime(startTime);
        selectedPerformance.setEndTime(endTime);

        performerName = null;
        startTime = null;
        endTime = null;
        this.endDate = tmpEnd;
    }

    public void saveFilePreviewContents() {
        if (null != currentFile) {
            filePreviewContents = currentFile.getContents();
        }
    }

    public String getFilePreviewContents() {
        return Base64.getEncoder().encodeToString(filePreviewContents);
    }

    public boolean isCurrentFileVideo() {
        return null != currentFile && currentFile.getContentType().startsWith("video");
    }

    public String currentFileName() {
        return null == currentFile ? "Preview" : currentFile.getFileName();
    }

    /*GETTERS AND SETTERS*/
    public UploadedFile getPendingUpload() {
        return pendingUpload;
    }

    public void setPendingUpload(UploadedFile pendingUpload) {
        this.pendingUpload = pendingUpload;
    }

    public List<File> getAllFiles() {
        return allFiles;
    }

    public void setAllFiles(List<File> allFiles) {
        this.allFiles = allFiles;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public int getPricePerDay() {
        return pricePerDay;
    }

    public void setPricePerDay(int pricePerDay) {
        this.pricePerDay = pricePerDay;
    }

    public int getPriceWholeFestival() {
        return priceWholeFestival;
    }

    public void setPriceWholeFestival(int priceWholeFestival) {
        this.priceWholeFestival = priceWholeFestival;
    }

    public int getNumTicketsPerDay() {
        return numTicketsPerDay;
    }

    public void setNumTicketsPerDay(int numTicketsPerDay) {
        this.numTicketsPerDay = numTicketsPerDay;
    }

    public List<Performance> getPerformances() {
        this.performances.sort((o1, o2) -> {
            Performance p1 = (Performance) o1;
            Performance p2 = (Performance) o2;

            int dateDiff = util.DateUtil.dateDayDiff(p2.getStartDate(), p1.getStartDate());
            if(0 == dateDiff){
                return util.DateUtil.dateHourDiff(p2.getStartDate(), p1.getStartDate());
            } else {
                return dateDiff;
            }
        });
        return performances;
    }

    public void setPerformances(List<Performance> performances) {
        this.performances = performances;
    }

    public List<Festival> getExistingFestivals() {
        if (null == existingFestivals) {
            existingFestivals = new ArrayList<>();
            Session session = null;
            Transaction tx = null;
            try {
                session = util.HibernateUtil.getSessionFactory().openSession();
                tx = session.beginTransaction();

                existingFestivals = session.createCriteria(Festival.class)
                        .add(Restrictions.eq("canceled", false)).list();
            } catch (HibernateException ex) {

            } finally {
                if (null != tx) {
                    tx.commit();
                }
                if (null != session) {
                    session.close();
                }
            }
        }
        return existingFestivals;
    }

    public List<Performer> getExistingPerformers() {
        if (null == existingPerformers) {
            Session session = null;
            Transaction tx = null;
            try {
                session = HibernateUtil.getSessionFactory().openSession();
                tx = session.beginTransaction();
                existingPerformers
                        = session.createCriteria(Performer.class
                        ).list();
            } catch (HibernateException ex) {

            } catch (Exception ex) {

            } finally {
                if (null != tx) {
                    tx.commit();
                }
                if (null != session) {
                    session.close();
                }
            }
        }
        return existingPerformers;
    }

    public List<SocialMedia> getAllSocialMedia() {
        return allSocialMedia;
    }

    public List<SocialMedia> getSelectedSocialMedia() {
        return selectedSocialMedia;
    }

    public void setSelectedSocialMedia(List<SocialMedia> selectedSocialMedia) {
        this.selectedSocialMedia = selectedSocialMedia;
    }

    public Map<String, String> getSocialMediaLinks() {
        return socialMediaLinks;
    }

    public void setSocialMediaLinks(Map<String, String> socialMediaLinks) {
        this.socialMediaLinks = socialMediaLinks;
    }

    public boolean isSelected(String media) {
        return selectedSocialMedia.stream().anyMatch((selected) -> (selected.getName().equals(media)));
    }

    public int getMaxTicketsPerUser() {
        return maxTicketsPerUser;
    }

    public void setMaxTicketsPerUser(int maxTicketsPerUser) {
        this.maxTicketsPerUser = maxTicketsPerUser;
    }

    public String getPerformerName() {
        return performerName;
    }

    public void setPerformerName(String performerName) {
        this.performerName = performerName;
    }

    public int getPerformanceFestDay() {
        return performanceFestDay;
    }

    public void setPerformanceFestDay(int performanceFestDay) {
        this.performanceFestDay = performanceFestDay;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Performance getSelectedPerformance() {
        return selectedPerformance;
    }

    public void setSelectedPerformance(Performance selectedPerformance) {
        this.selectedPerformance = selectedPerformance;
    }

    public UploadedFile getCurrentFile() {
        return currentFile;
    }

    public void setCurrentFile(UploadedFile currentFile) {
        this.currentFile = currentFile;
    }

    public List<UploadedFile> getUploadedFiles() {
        return uploadedFiles;
    }

    public void setUploadedFiles(List<UploadedFile> uploadedFiles) {
        this.uploadedFiles = uploadedFiles;
    }

    public File getCurrentFileToPreview() {
        return currentFileToPreview;
    }

    public void setCurrentFileToPreview(File currentFileToPreview) {
        this.currentFileToPreview = currentFileToPreview;
        if (null != currentFileToPreview && currentFileToPreview.getIsVideo()) {
            videoToPreview = new Video(currentFileToPreview.getFestivalName(), currentFileToPreview.getName());
        } else {
            videoToPreview = null;
        }
    }

    public Video getVideoToPreview() {
        return videoToPreview;
    }

    public void setVideoToPreview(Video videoToPreview) {
        this.videoToPreview = videoToPreview;
    }
}
