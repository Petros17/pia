/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataModels;

import entity.Festival;
import entity.Performance;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author Petar
 */
public class FestivalScheduleModel implements Serializable {

    private List<FestivalDay> festivalDays;

    public FestivalScheduleModel(Festival festival) {
        festivalDays = new ArrayList<>();
        int diff = util.DateUtil.dateDayDiff(festival.getStartDate(), festival.getEndDate()) + 1;
        List<Performance> performances = new ArrayList<>(festival.getPerformances());
        for (int i = 1; i <= diff; i++) {
            Date day = util.DateUtil.addToDate(festival.getStartDate(), i - 1);
            List<Performance> dayPerformances = performances.stream()
                    .filter((perf) -> !perf.getStartDate().after(day) && !perf.getStartDate().before(day))
                    .collect(Collectors.toList());
            
            FestivalDay festDay = new FestivalDay(day, dayPerformances, i);
            festivalDays.add(festDay);
        }
    }

    public FestivalScheduleModel(List<Performance> performances, Date startDate, Date endDate){
        festivalDays = new ArrayList<>();
        int diff = util.DateUtil.dateDayDiff(startDate, endDate) + 1;
        for (int i = 1; i <= diff; i++) {
            Date day = util.DateUtil.addToDate(startDate, i - 1);
            List<Performance> dayPerformances = performances.stream()
                    .filter((perf) -> !perf.getStartDate().after(day) && !perf.getStartDate().before(day))
                    .collect(Collectors.toList());
            
            FestivalDay festDay = new FestivalDay(day, dayPerformances, i);
            festivalDays.add(festDay);
        }
    }
    
    public List<FestivalDay> getFestivalDays() {
        return festivalDays;
    }

    public void setFestivalDays(List<FestivalDay> festivalDays) {
        this.festivalDays = festivalDays;
    }
    
    
}
