/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataModels;

import java.io.Serializable;

/**
 *
 * @author Petar
 */
public class Video implements Serializable {
    private String fileName;
    private String type;
    private String festivalName;

    public Video() {
    }

    public Video(String festivalName, String fileName) {
        this.fileName = fileName;
        this.festivalName = festivalName;
        
        String[] parts = fileName.split("\\.");
        type = parts[parts.length - 1];
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }
    
    
}
