/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userControllers;

import com.mysql.jdbc.StringUtils;
import entity.Comment;
import entity.Festival;
import entity.Reservation;
import entity.User;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Petar
 */
public class CommentController implements Serializable {

    private List<Comment> comments;
    private Festival festival;

    private String comment;
    private int rating = 5;

    /**
     * Creates a new instance of CommentController
     */
    public CommentController() {
        festival = (Festival) ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true)).getAttribute("currentFestival");
        if (null != festival) {
            Session session = null;
            Transaction tx = null;
            try {
                session = util.HibernateUtil.getSessionFactory().openSession();
                tx = session.beginTransaction();

                comments = session.createCriteria(Comment.class)
                        .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                        .add(Restrictions.eq("festivalName", festival.getName()))
                        .addOrder(Order.desc("date"))
                        .list();

            } catch (HibernateException ex) {

            } finally {
                if (null != tx) {
                    tx.commit();
                }
                if (null != session) {
                    session.close();
                }
            }
        }
    }

    public List<Comment> getComments() {
        return comments;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public void leaveComment() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);

        User currentUser = (User) ((HttpSession) context.getExternalContext().getSession(true)).getAttribute("currentUser");
        if (null == currentUser || currentUser.isIsAdmin()) {
            return;
        }
        
        if(StringUtils.isNullOrEmpty(comment)){
            context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "Comment text is required"));
            return;
        }

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            session.refresh(currentUser);

            List<Reservation> purchases = (new ArrayList<>(currentUser.getReservations()))
                    .stream().filter((res) -> res.isPurchased() && res.getFestival().getId().equals(festival.getId()))
                    .collect(Collectors.toList());

            if (purchases.isEmpty()) {
                context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "You cannot leave a comment if you haven't purchased a ticket for this festival!"));
                tx.commit();
                return;
            }
            
            festival = (Festival) session.get(Festival.class, festival.getId());
            Hibernate.initialize(festival.getComments());
            List<Comment> thisFestComments = (new ArrayList<>(festival.getComments()))
                    .stream().filter((com) -> com.getUser().getUsername().equals(currentUser.getUsername()))
                    .collect(Collectors.toList());
            
            if(!thisFestComments.isEmpty()){
                context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "You cannot leave more than one comment!"));
                tx.commit();
                return;
            }
            
            Comment newComment = new Comment(festival, currentUser, festival.getName(), rating, comment, Calendar.getInstance().getTime());
            session.persist(newComment);
            Hibernate.initialize(currentUser.getComments());
            
            currentUser.getComments().add(newComment);
            festival.getComments().add(newComment);
            
            tx.commit();
            session.flush();
            
            comments.add(0, newComment);
            comment = null;
            rating = 5;
            
            context.addMessage(null, new FacesMessage("Success", "Your comment is saved"));
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "There has been an error, please try again"));
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    public int getFestRating(){
        double newRating = 0;
        for(Comment com: comments){
            newRating += com.getRating();
        }
        if(comments.size() > 0){
            newRating /= comments.size();
        }
        
        if(newRating + 0.5 >= Math.ceil(newRating)) newRating = Math.ceil(newRating);
        else newRating = Math.floor(newRating);
        
        return (int) newRating;
    }
}
