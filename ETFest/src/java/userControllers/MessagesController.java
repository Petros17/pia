/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userControllers;

import entity.User;
import entity.UserMessage;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Petar
 */
public class MessagesController implements Serializable {

    private List<UserMessage> messages;
    private int unreadMessages;
    private User user;
    
    private UserMessage selected;
    
    /**
     * Creates a new instance of MessagesController
     */
    public MessagesController() {
        update();
    }
    
    public final void update(){
        messages = new ArrayList<>();
        Session session = null;
        Transaction tx = null;
        try{
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            
            user = (User) ((HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true)).getAttribute("currentUser");
            
            messages = session.createCriteria(UserMessage.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("user", user))
                    .addOrder(Order.desc("id"))
                    .list();
                    
            unreadMessages = messages.stream().filter((msg) -> !msg.isMessageRead()).collect(Collectors.toList()).size();
        } catch(HibernateException ex){
            
        } catch(Exception ex){
            
        } finally {
            if(null != tx){
                tx.commit();
            }
            if(null != session){
                session.close();
            }
        }
    }
    
    public void read(){
        if(null == selected){
            return;
        }
        Session session = null;
        Transaction tx = null;
        try{
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            
            selected.setMessageRead(true);
            session.update(selected);
            unreadMessages--;
            
            tx.commit();
            session.flush();
        } catch(HibernateException ex){
            if(null != tx){
                tx.rollback();
            }
        } catch(Exception ex){
            if(null != tx){
                tx.rollback();
            }
        } finally {
            if(null != session){
                session.close();
            }
        }
    }
    
    public void delete(){
        if(null == selected){
            return;
        }
        FacesContext context = FacesContext.getCurrentInstance();
        Session session = null;
        Transaction tx = null;
        try{
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            
            user.getUserMessages().remove(selected);
            selected.getMessage().getUserMessages().remove(selected);
            session.delete(selected);
            
            tx.commit();
            session.flush();
            
            messages.remove(selected);
            selected = null;
            
            context.addMessage(null, new FacesMessage("Success", "You have successfully deleted the message"));
        } catch(HibernateException ex){
            if(null != tx){
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "There has been an error, please try again"));
        } catch(Exception ex){
            if(null != tx){
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "There has been an error, please try again"));
        } finally {
            if(null != session){
                session.close();
            }
        }
    }

    public List<UserMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<UserMessage> messages) {
        this.messages = messages;
    }

    public int getUnreadMessages() {
        return unreadMessages;
    }

    public void setUnreadMessages(int unreadMessages) {
        this.unreadMessages = unreadMessages;
    }

    public UserMessage getSelected() {
        return selected;
    }

    public void setSelected(UserMessage selected) {
        this.selected = selected;
    }
}
