/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userControllers;

import com.mysql.jdbc.StringUtils;
import entity.RegistrationRequest;
import entity.User;
import java.io.IOException;
import javax.faces.application.FacesMessage;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Petar
 */
public class RegisterController {

    private String errorMessage;
    private String username;
    private String password;
    private String confirmPassword;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;

    /**
     * Creates a new instance of RegisterController
     */
    public RegisterController() {
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void register() {
        errorMessage = null;
        boolean requestSent = false;
        RequestContext requestContext = RequestContext.getCurrentInstance();
        requestContext.addCallbackParam("requestSent", false);
        if (StringUtils.isNullOrEmpty(username) || StringUtils.isNullOrEmpty(password) || StringUtils.isNullOrEmpty(confirmPassword)
                || StringUtils.isNullOrEmpty(firstName) || StringUtils.isNullOrEmpty(lastName) || StringUtils.isNullOrEmpty(email) || StringUtils.isNullOrEmpty(phone)) {
            errorMessage = "All fields are required";
            return;
        }

        if (!password.equals(confirmPassword)) {
            errorMessage = "Password and confirmation do not match";
            return;
        }

        if (null != (errorMessage = util.PasswordUtil.validatePassword(password))) {
            return;
        }

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            Query userQuery = session.createQuery("from User as usr where usr.username= :uname");
            userQuery.setParameter("uname", username);
            User existingUser = (User) userQuery.uniqueResult();
            if (null != existingUser) {
                errorMessage = "Username already exists";
                tx.commit();
                return;
            }

            existingUser = (User) session.createCriteria(User.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("email", email))
                    .uniqueResult();

            if (null != existingUser) {
                errorMessage = "There's an account associated with this email";
                tx.commit();
                return;
            }

            userQuery = session.createQuery("from RegistrationRequest as req where req.username= :uname");
            userQuery.setParameter("uname", username);
            RegistrationRequest existingRequest = (RegistrationRequest) userQuery.uniqueResult();
            if (null != existingRequest) {
                errorMessage = "Username already exists";
                tx.commit();
                return;
            }

            userQuery = session.createQuery("from RegistrationRequest as req where req.email= :email");
            userQuery.setParameter("email", email);
            existingRequest = (RegistrationRequest) userQuery.uniqueResult();
            if (null != existingRequest) {
                errorMessage = "Email already exists";
                tx.commit();
                return;
            }
            
            RegistrationRequest regRequest = new RegistrationRequest(username, util.PasswordUtil.getMD5(password), firstName, lastName, phone, email);
            session.persist(regRequest);

            tx.commit();
            session.flush();
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
            errorMessage = "There has been an error, please try again";
            return;
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
            errorMessage = "There has been an error, please try again";
            return;
        } finally {
            if (null != session) {
                session.close();
            }
        }

        FacesContext context = FacesContext.getCurrentInstance();

        context.addMessage(
                null, new FacesMessage("Success!", "Your request is pending approval"));
        context.getExternalContext()
                .getFlash().setKeepMessages(true);

        try {
            ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
            ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
        } catch (IOException ex) {
            context.getApplication().getNavigationHandler().handleNavigation(context, null, "index?faces-redirect=true");
        }
    }
}
