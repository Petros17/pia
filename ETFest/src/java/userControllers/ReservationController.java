/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userControllers;

import entity.AvailableTickets;
import entity.Festival;
import entity.Reservation;
import entity.User;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author Petar
 */
public class ReservationController {

    private final Festival festival;
    private final List<Integer> days;
    public static final int SINGLE_DAY_TICKET = 1;
    public static final int WHOLE_FEST_TICKET = 2;

    private int type;
    private int day;
    private int amount = 1;

    /**
     * Creates a new instance of ReservationController
     */
    public ReservationController() {
        HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        festival = (Festival) httpSession.getAttribute("currentFestival");

        days = new ArrayList<>();
        int dayDiff = util.DateUtil.dateDayDiff(festival.getStartDate(), festival.getEndDate()) + 1;
        for (int i = 1; i <= dayDiff; i++) {
            days.add(i);
        }
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<Integer> getDays() {
        return days;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public void reserve() {
        FacesContext context = FacesContext.getCurrentInstance();
        if (!canReserve()) {
            context.getExternalContext().getFlash().setKeepMessages(true);
            return;
        }

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            HttpSession httpSession = (HttpSession) context.getExternalContext().getSession(true);
            User user = (User) httpSession.getAttribute("currentUser");
            Reservation res = new Reservation(festival, user, day, WHOLE_FEST_TICKET == type, amount, false /*purchased*/, Calendar.getInstance().getTime(), false/*expired*/);
            
            session.refresh(festival);
            Hibernate.initialize(festival.getReservations());
            festival.getReservations().add(res);
            
            user.getReservations().add(res);
            session.persist(res);

            if (WHOLE_FEST_TICKET == type) {
                for (AvailableTickets elem : festival.getAvailableTicketses()) {
                    elem.setTicketsLeft(elem.getTicketsLeft() - amount);
                    session.update(elem);
                }
            } else {
                AvailableTickets elem = festival.getAvailableTicketses().stream().filter((aT) -> aT.getDay() == day).findFirst().get();
                elem.setTicketsLeft(elem.getTicketsLeft() - amount);
                session.update(elem);
            }

            context.addMessage(null, new FacesMessage("Success", "Your reservations are saved!"));
            tx.commit();
            session.flush();
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }

    private boolean canReserve() {
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession httpSession = (HttpSession) context.getExternalContext().getSession(true);
        User user = (User) httpSession.getAttribute("currentUser");
        if (null == user || user.isIsBlocked()) {
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "You do not have the permission to reserve!"));
            return false;
        }

        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            List<Reservation> reservations = session.createCriteria(Reservation.class)
                    .setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                    .add(Restrictions.eq("festival", festival))
                    .add(Restrictions.eq("user", user))
                    .list();

            long wholeFestTickets = 0;
            long currentDayTickets = 0;

            for (Reservation res : reservations) {
                if (res.isForWholeFestival()) {
                    wholeFestTickets += res.getNumOfTickets();
                } else if(res.getDay() == day) {
                    currentDayTickets += res.getNumOfTickets();
                }
            }

            if (wholeFestTickets + currentDayTickets + amount > festival.getMaxTicketsPerUser()) {
                context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "You already hit your limit for this fest!"));
                return false;
            }

            if (WHOLE_FEST_TICKET == type) {
                for (AvailableTickets elem : festival.getAvailableTicketses()) {
                    if (elem.getTicketsLeft() < amount) {
                        context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "There are not enough tickets for day " + elem.getDay()));
                        return false;
                    }
                }
            } else {
                AvailableTickets tick = festival.getAvailableTicketses().stream().filter((elem) -> elem.getDay() == day).findFirst().get();
                if (tick.getTicketsLeft() < amount) {
                    context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "There are not enough tickets left"));
                    return false;
                }
            }

            return true;
        } catch (HibernateException ex) {
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again!"));
            return false;
        } catch (Exception ex) {
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again!"));
            return false;
        } finally {
            if (null != tx) {
                tx.commit();
            }
            if (null != session) {
                session.close();
            }
        }
    }
}
