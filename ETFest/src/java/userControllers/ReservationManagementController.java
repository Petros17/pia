/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userControllers;

import entity.Reservation;
import entity.User;
import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author Petar
 */
public class ReservationManagementController {

    private User user;
    private List<Reservation> reservations;
    private Reservation selected;

    /**
     * Creates a new instance of ReservationManagementController
     */
    public ReservationManagementController() {
        HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
        user = (User) httpSession.getAttribute("currentUser");
        Session session = null;
        Transaction tx = null;
        try{
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.refresh(user);
            Hibernate.initialize(user.getReservations());
            tx.commit();
        } catch(HibernateException ex){
            
        } finally {
            if(null != session){
                session.close();
            }
        }
        reservations = new ArrayList<>(user.getReservations());
        reservations.sort((o1, o2) -> {
            Reservation res1 = (Reservation) o1;
            Reservation res2 = (Reservation) o2;
            
            boolean isAfter = res1.getFestival().getStartDate().after(res2.getFestival().getStartDate());
            boolean isBefore = res1.getFestival().getStartDate().before(res2.getFestival().getStartDate());
            
            return (isAfter ? 1 : isBefore ? -1 : res2.getId() - res1.getId());
        });
    }

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public Reservation getSelected() {
        return selected;
    }

    public void setSelected(Reservation selected) {
        this.selected = selected;
    }

    public void delete() {
        if (null == selected) {
            return;
        }

        FacesContext context = FacesContext.getCurrentInstance();
        
        if(selected.isExpired() || selected.isPurchased()){
            context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "You cannot cancel expired or purchased reservations"));
            return;
        }
        
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            
            session.refresh(selected.getFestival());
            Hibernate.initialize(selected.getFestival().getReservations());
            
            releaseFestTickets(selected, session);
            reservations.remove(selected);
            
            selected = (Reservation) session.get(Reservation.class, selected.getId());
            session.delete(selected);

            tx.commit();
            session.flush();
            
            Hibernate.initialize(user.getReservations());
            selected.getFestival().getReservations().remove(selected);
            user.getReservations().remove(selected);
            
            selected = null;
            
            context.addMessage(null, new FacesMessage("Success", "You have successfully canceled the reservation"));
        } catch (HibernateException ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong. Please, try again"));
        } catch (Exception ex) {
            if (null != tx) {
                tx.rollback();
            }
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong. Please, try again"));
        } finally {
            if (null != session) {
                session.close();
            }
        }

        context.getExternalContext().getFlash().setKeepMessages(true);
    }

    public String description(Reservation res){
        if(res.isForWholeFestival()){
            return "Whole festival";
        } else {
            return "Day " + res.getDay();
        }
    }
    
    private void releaseFestTickets(Reservation res, Session session) {
        res.getFestival().getAvailableTicketses().forEach((elem) -> {
            if (res.isForWholeFestival() || (elem.getDay() == res.getDay())) {
                elem.setTicketsLeft(elem.getTicketsLeft() + res.getNumOfTickets());
                session.update(elem);
            }
        });
    }
    
    public void viewFestival(){
        if(null == selected) {
            return;
        }
        
        FacesContext context = FacesContext.getCurrentInstance();
        HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
        session.setAttribute("currentFestival", selected.getFestival());
        
        context.getApplication().getNavigationHandler().handleNavigation(context, null, "festival?faces-redirect=true");
    }
}
