/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userControllers;

import adminControllers.AddFestivalController;
import entity.Festival;
import entity.File;
import entity.Reservation;
import entity.User;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import static javax.faces.application.FacesMessage.SEVERITY_WARN;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Petar
 */
public class UserFileUploadController implements Serializable {

    private UploadedFile currentFile;
    private byte[] filePreviewContents;
    private static final long VIDEO_SIZE_LIMIT = 100 * 1024 * 1024;

    /**
     * Creates a new instance of UserFileUploadController
     */
    public UserFileUploadController() {
    }

    public UploadedFile getCurrentFile() {
        return currentFile;
    }

    public void setCurrentFile(UploadedFile currentFile) {
        this.currentFile = currentFile;
    }

    public void preview(FileUploadEvent event) {
        setCurrentFile(event.getFile());
        if (null != currentFile) {
            filePreviewContents = currentFile.getContents();
        }
    }

    public String getFilePreviewContents() {
        return Base64.getEncoder().encodeToString(filePreviewContents);
    }

    public void upload() {
        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);

        if (null == currentFile) {
            context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "You must choose a file to upload"));
            return;
        }

        Session session = null;
        Transaction tx = null;
        try {
            HttpSession httpSession = (HttpSession) context.getExternalContext().getSession(true);
            User currentUser = (User) httpSession.getAttribute("currentUser");
            Festival festival = (Festival) httpSession.getAttribute("currentFestival");
            if (null == currentUser || null == festival) {
                return;
            }
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();
            session.refresh(currentUser);

            List<Reservation> purchases = (new ArrayList<>(currentUser.getReservations()))
                    .stream().filter((res) -> res.isPurchased() && res.getFestival().getId().equals(festival.getId()))
                    .collect(Collectors.toList());

            if (purchases.isEmpty()) {
                context.addMessage(null, new FacesMessage(SEVERITY_WARN, "Warning", "You cannot upload if you haven't purchased a ticket for this festival!"));
                tx.commit();
                return;
            }

            util.FileUtil.persistFile(session, festival, currentFile, currentUser);
            tx.commit();
            session.flush();
            context.addMessage(null, new FacesMessage("Success", "Your uploaded file is pending approval"));
        } catch (HibernateException ex) {
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again"));
            if(null != tx){
                tx.rollback();
            }
        } finally {
            if (null != session) {
                session.close();
            }
        }
    }


    public String getCurrentFileName() {
        if (null != currentFile) {
            return currentFile.getFileName();
        }
        return "";
    }

    public boolean isCurrentFileVideo() {
        return null != currentFile && currentFile.getContentType().startsWith("video");
    }

}
