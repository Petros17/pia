/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userControllers;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken.Payload;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.googleapis.util.Utils;
import com.mysql.jdbc.StringUtils;
import entity.Reservation;
import entity.User;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import javax.faces.application.FacesMessage;
import static javax.faces.application.FacesMessage.SEVERITY_ERROR;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.NoResultException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.primefaces.context.RequestContext;

/**
 *
 * @author Petar
 */
public class UserLoginController {

    private static final String CLIENT_ID = "521349229736-37ofti9dl080g43stko335kotj79c02i.apps.googleusercontent.com";
    private String username;
    private String password;
    private String errorMessage;
    private String idTokenString;

    /**
     * Creates a new instance of UserLoginController
     */
    public UserLoginController() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public void login() {
        if (StringUtils.isNullOrEmpty(idTokenString)) {
            databaseLogin();
        } else {
            socialNetworkLogin();
        }
    }

    private void socialNetworkLogin() {

        FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().setKeepMessages(true);
        try {
            GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(Utils.getDefaultTransport(), Utils.getDefaultJsonFactory())
                    .setAudience(Collections.singletonList(CLIENT_ID))
                    .build();

            GoogleIdToken idToken = verifier.verify(idTokenString);
            if (idToken != null) {
                Payload payload = idToken.getPayload();
                // Get profile information from payload
                String email = payload.getEmail();
                String userId = payload.getSubject();
                String name = (String) payload.get("name");
                String familyName = (String) payload.get("family_name");
                String givenName = (String) payload.get("given_name");

                Session session = null;
                Transaction tx = null;
                try {
                    session = util.HibernateUtil.getSessionFactory().openSession();
                    tx = session.beginTransaction();

                    User user = null;

                    try {
                        user = (User) session.createCriteria(User.class).setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY)
                                .add(Restrictions.eq("email", email))
                                .uniqueResult();

                        if (null == user) {
                            user = new User(userId, util.PasswordUtil.getMD5(userId), email, "", givenName, familyName, false, false, 0, true);
                            session.persist(user);
                        }
                    } catch (HibernateException ex) {
                        tx.commit();
                        context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Multiple users registered with same email. Login not possible via social networks"));
                        return;
                    }
                    user.setLastLogin(Calendar.getInstance().getTime());
                    session.update(user);

                    HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
                    httpSession.setAttribute("currentUser", user);

                    checkReservationExpiration(session, user);
                    tx.commit();
                    session.flush();

                    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                    ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
                } catch (HibernateException ex) {
                    if (null != tx) {
                        tx.rollback();
                    }
                    context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again"));
                } finally {
                    if (null != session) {
                        session.close();
                    }
                    idTokenString = null;
                }
            } else {
                context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Invalid login token"));
            }
        } catch (GeneralSecurityException | IOException ex) {
            context.addMessage(null, new FacesMessage(SEVERITY_ERROR, "Error", "Something went wrong, please try again"));
        }
    }

    private void databaseLogin() {
        boolean loggedIn = false;
        RequestContext requestContext = RequestContext.getCurrentInstance();
        errorMessage = null;
        if (StringUtils.isNullOrEmpty(username) || StringUtils.isNullOrEmpty(password)) {
            errorMessage = "All fields are required";
            requestContext.addCallbackParam("loggedIn", loggedIn);
            return;
        }
        Session session = null;
        Transaction tx = null;
        try {
            session = util.HibernateUtil.getSessionFactory().openSession();
            tx = session.beginTransaction();

            String passwordMD5 = util.PasswordUtil.getMD5(password);
            Query query = session.createQuery("from User as usr where usr.username= :uname and usr.password='" + passwordMD5 + "' and is_admin=0");
            query.setParameter("uname", username);
            User user = (User) query.uniqueResult();
            if (null == user) {
                throw new NoResultException();
            }
            user.setLastLogin(Calendar.getInstance().getTime());
            session.update(user);

            HttpSession httpSession = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(true);
            httpSession.setAttribute("currentUser", user);
            loggedIn = true;
            checkReservationExpiration(session, user);
            tx.commit();
            session.flush();
        } catch (NoResultException ex) {
            errorMessage = "Invalid login parameters";
        } catch (HibernateException ex) {
            errorMessage = "There has been an error. Please retry";
        } finally {
            if (null != session) {
                session.close();
            }
        }
        requestContext.addCallbackParam("loggedIn", loggedIn);
        if (loggedIn) {
            try {
                ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
                ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
            } catch (IOException ex) {
                FacesContext facesContext = FacesContext.getCurrentInstance();
                facesContext.getApplication().getNavigationHandler().handleNavigation(facesContext, null, "index?faces-redirect=true");
            }
        }
    }

    private void checkReservationExpiration(Session session, User user) {
        if (user.isIsBlocked()) {
            return;
        }
        Hibernate.initialize(user.getReservations());
        List<Reservation> reservations = user.getReservations().stream().filter((res) -> !res.isExpired() && !res.isPurchased()).collect(Collectors.toList());
        Date now = Calendar.getInstance().getTime();
        int initialMissed = user.getMissedReservations();
        reservations.forEach((res) -> {
            if (util.DateUtil.dateHourDiff(res.getDate(), now) > 48) {
                res.setExpired(true);
                releaseFestTickets(res, session);
                user.setMissedReservations(user.getMissedReservations() + 1);
                session.update(res);
            }
        });

        if (initialMissed != user.getMissedReservations()) {
            if (user.getMissedReservations() > 2) {
                user.setIsBlocked(true);
            }
            session.update(user);
        }
    }

    private void releaseFestTickets(Reservation res, Session session) {
        res.getFestival().getAvailableTicketses().forEach((elem) -> {
            if (res.isForWholeFestival() || (elem.getDay() == res.getDay())) {
                elem.setTicketsLeft(elem.getTicketsLeft() + res.getNumOfTickets());
                session.update(elem);
            }
        });
    }

    public String getIdTokenString() {
        return idTokenString;
    }

    public void setIdTokenString(String idTokenString) {
        this.idTokenString = idTokenString;
    }

}
