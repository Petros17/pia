/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userControllers;

import javax.faces.context.FacesContext;

/**
 *
 * @author Petar
 */
public class UserLogoutController {

    /**
     * Creates a new instance of UserLogoutController
     */
    public UserLogoutController() {
    }
    
    public String logout(){
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        return "index?faces-redirect=true";
    }
}
