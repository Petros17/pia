/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import com.csvreader.CsvReader;
import dataModels.FestivalModel;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Petar
 */
public class CSVParser {

    private CsvReader csvReader;
    private UploadedFile file;
    private FestivalModel model;

    public CSVParser(UploadedFile file) {
        this.file = file;
        this.model = new FestivalModel();
    }

    public FestivalModel parse() throws FileParseException {
        ByteArrayInputStream stream = null;
        BufferedReader reader = null;
        try {
            stream = new ByteArrayInputStream(file.getContents());
            reader = new BufferedReader(new InputStreamReader(stream));
            csvReader = new CsvReader(reader);

            parseBasicInfo();
            parseTicketInfo();
            parsePerformanceInfo();
            parseSocialLinks();
        } catch (FileParseException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new FileParseException("Unexpected error. Please, try again.");
        } finally {
            if (null != csvReader) {
                csvReader.close();
            }
            try {
                reader.close();
            } catch (IOException | NullPointerException ex) {
            }

            try {
                stream.close();
            } catch (IOException | NullPointerException ex) {
            }
        }

        return model;
    }

    private void parseBasicInfo() throws FileParseException {
        try {
            csvReader.readHeaders();
            if (!csvReader.readRecord()) {
                throw new FileParseException("Festival basic information missing");
            }
            try {
                String name = csvReader.get("Festival");
                String place = csvReader.get("Place");
                String startDateString = csvReader.get("StartDate");
                String endDateString = csvReader.get("EndDate");

                model.setName(name);
                model.setLocation(place);

                DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                Date startDate = format.parse(startDateString);
                Date endDate = format.parse(endDateString);
                model.setStartDate(startDate);
                model.setEndDate(endDate);
            } catch (IOException ex) {
                throw new FileParseException("Required basic information not found");
            } catch (ParseException ex) {
                throw new FileParseException("Illegal date format for start/end date");
            }
        } catch (IOException ex) {
            throw new FileParseException("Error parsing the file!");
        }
    }

    private void parseTicketInfo() throws FileParseException {
        try {
            csvReader.readHeaders();
            if (!csvReader.readRecord()) {
                throw new FileParseException("Festival ticket price information missing");
            }
            String pricePerDayString = "";
            try {
                pricePerDayString = csvReader.get("Price");
            } catch (IOException ex) {
                throw new FileParseException("Required ticket price information not found");
            }
            if (!csvReader.readRecord()) {
                throw new FileParseException("Festival ticket price information missing");
            }
            String priceWholeFestString = "";
            try {
                priceWholeFestString = csvReader.get("Price");
                int pricePerDay = Integer.parseInt(pricePerDayString);
                int priceWholeFest = Integer.parseInt(priceWholeFestString);

                model.setPricePerDay(pricePerDay);
                model.setPriceWholeFestival(priceWholeFest);
            } catch (IOException ex) {
                throw new FileParseException("Required ticket price information not found");
            } catch (NumberFormatException ex) {
                throw new FileParseException("Invalid ticket price");
            }

            csvReader.readHeaders();
            if (!csvReader.readRecord()) {
                throw new FileParseException("Festival ticket availability information missing");
            }
            String amountPerUserString = "";
            try {
                amountPerUserString = csvReader.get("Amount");
            } catch (IOException ex) {
                throw new FileParseException("Required ticket availability information not found");
            }
            if (!csvReader.readRecord()) {
                throw new FileParseException("Festival ticket availability information missing");
            }
            String amountPerDayString = "";
            try {
                amountPerDayString = csvReader.get("Amount");
                int amountPerUser = Integer.parseInt(amountPerUserString);
                int amountPerDay = Integer.parseInt(amountPerDayString);

                model.setMaxTicketsPerUser(amountPerUser);
                model.setNumTicketsPerDay(amountPerDay);
            } catch (IOException ex) {
                throw new FileParseException("Required ticket availability information not found");
            } catch (NumberFormatException ex) {
                throw new FileParseException("Invalid ticket amount");
            }
        } catch (IOException ex) {
            throw new FileParseException("Error parsing the file!");
        }
    }

    private void parsePerformanceInfo() throws FileParseException {
        try {
            csvReader.readHeaders();
            while (csvReader.readRecord()) {
                String performer = csvReader.get("Performer");
                if (performer.equals("Social Network")) {
                    break;
                }
                String startDateString = csvReader.get("StartDate");
                String endDateString = csvReader.get("EndDate");
                String startTimeString = csvReader.get("StartTime");
                String endTimeString = csvReader.get("EndTime");

                try {
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                    Date startDate = dateFormat.parse(startDateString);
                    Date endDate = dateFormat.parse(endDateString);

                    if (startDate.before(model.getStartDate())) {
                        throw new FileParseException("Performance start date is before festival start date");
                    }

                    if (startDate.after(model.getEndDate())) {
                        throw new FileParseException("Performance start date is after festival end date");
                    }

                    DateFormat timeFormat = new SimpleDateFormat("KK:mm:ss a", Locale.ENGLISH);
                    Date startTime = timeFormat.parse(startTimeString);
                    Date endTime = timeFormat.parse(endTimeString);

                    int performanceDay = DateUtil.dateDayDiff(model.getStartDate(), startDate) + 1;

                    model.setPerformanceFestDay(performanceDay);
                    model.setStartTime(startTime);
                    model.setEndTime(endTime);
                } catch (ParseException ex) {
                    throw new FileParseException("Invalid date/time format");
                }
                model.setPerformerName(performer);

                model.saveCurrentPerformance();
            }
        } catch (IOException ex) {
            throw new FileParseException("Invalid performance data");
        }
    }

    private void parseSocialLinks() throws FileParseException {
        csvReader.setHeaders(new String[]{"Social Network", "Link"});
        try {
            while (csvReader.readRecord()) {
                String network = csvReader.get("Social Network");
                String link = csvReader.get("Link");
                model.getSocialMediaLinks().put(network, link);
            }
        } catch (IOException ex) {
            throw new FileParseException("Error parsing social links");
        }
    }
}
