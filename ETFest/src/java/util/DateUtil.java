/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Petar
 */
public final class DateUtil {
    public static int dateDayDiff(Date first, Date second){
        long msDiff = second.getTime() - first.getTime();
        return (int) Math.ceil(1.0 * msDiff / 1000 / 60 / 60 / 24);
    }
    
    public static int dateHourDiff(Date first, Date second){
        long msDiff = second.getTime() - first.getTime();
        return (int) Math.ceil(1.0 * msDiff / 1000 / 60 / 60);
    }
    
    public static Date addToDate(Date date, int days){
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(date.getTime() + days * 24 * 60 * 60 * 1000);
        return cal.getTime();
    }
}
