/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

/**
 *
 * @author Petar
 */
public class FileParseException extends Exception {

    protected String message;
    
    public FileParseException(String message){
        this.message = message;
    }
    
    @Override
    public String getMessage() {
        return this.message; //To change body of generated methods, choose Tools | Templates.
    }
    
}
