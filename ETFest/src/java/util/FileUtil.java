/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import dataModels.Video;
import entity.Festival;
import entity.File;
import entity.User;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Calendar;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Petar
 */
public class FileUtil {

    public static final String VIDEO_FOLDER_PATH = "F:\\Projekti\\PIA\\pia\\ETFest\\build\\web\\videos";

    public static void persistFile(Session session, Festival festival, UploadedFile uploadedFile, User user) {
        if (null == uploadedFile) {
            return;
        }
        String[] parts = uploadedFile.getContentType().split("/");

        String fileName = uploadedFile.getFileName();
        if (fileName.length() > 255) {
            fileName = fileName.substring(0, 200);
            fileName += "." + parts[1];
        }

        fileName = generateFileHash(fileName) + fileName;
        String[] fileNameParts = fileName.split("\\.");
        String extension = fileNameParts[fileNameParts.length - 1];
        fileName = fileNameParts[0];
        for (int i = 1; i < fileNameParts.length - 1; i++) {
            fileName += "." + fileNameParts[i];
        }

        File file = new File();
        file.setFestivalName(festival.getName());
        file.setSize((int) uploadedFile.getSize());
        file.setUser(user);

        if (parts[0].equals("video")) {
            fileName = saveVideo(uploadedFile, fileName, extension, festival.getName());
            if (null == fileName) {
                return;
            }
            file.setName(fileName);
            file.setIsVideo(Boolean.TRUE);
            file.setContent("Video".getBytes());
        } else {
            file.setName(uploadedFile.getFileName());
            file.setContent(uploadedFile.getContents());
            file.setIsVideo(Boolean.FALSE);
        }
        file.setIsApproved(null == user);
        session.persist(file);
        if (null != user) {

            try {
                session.refresh(user);
                Hibernate.initialize(user.getFiles());
                user.getFiles().add(file);
            } catch (HibernateException ex) {

            }
        }
    }

    private static String saveVideo(UploadedFile video, String fileName, String extension, String festName) {
        try {
            InputStream stream = video.getInputstream();
            String path = VIDEO_FOLDER_PATH + "\\" + festName + "\\";
            Path folder = Paths.get(path);
            Path file;

            try {
                file = Files.createTempFile(folder, fileName, "." + extension);
            } catch (IOException ex) {
                new java.io.File(path).mkdirs();
                file = Files.createTempFile(folder, fileName, "." + extension);
            }
            Files.copy(stream, file, StandardCopyOption.REPLACE_EXISTING);
            return file.getFileName().toString();
        } catch (IOException ex) {
            String msg = ex.getMessage();
        }
        return null;
    }

    private static int generateFileHash(String fileName) {
        String hashVal = fileName + String.valueOf(Calendar.getInstance().getTimeInMillis());
        return hashVal.hashCode();
    }

    public static void deleteVideo(Video selectedVideo) throws IOException {
        String stringPath = VIDEO_FOLDER_PATH + "\\" + selectedVideo.getFestivalName() + "\\" + selectedVideo.getFileName();
        Path path = Paths.get(stringPath);

        Files.delete(path);
    }
}
