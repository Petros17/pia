/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import dataModels.SocialMedia;
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ApplicationScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.inject.Named;

/**
 *
 * @author Petar
 */
@Named
@ApplicationScoped
@FacesConverter(forClass = SocialMedia.class, value = "imageConverter")
public class ImageConverter implements Converter {

    public static final List<SocialMedia> social_media;

    static {
        social_media = new ArrayList<>();
        social_media.add(new SocialMedia(0, "Facebook", "facebook.png"));
        social_media.add(new SocialMedia(1, "Twitter", "twitter.png"));
        social_media.add(new SocialMedia(2, "Instagram", "instagram.png"));
        social_media.add(new SocialMedia(3, "YouTube", "youtube.png"));
    }

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (null != value && value.trim().length() > 0) {
            try {
                return social_media.get(Integer.parseInt(value));
            } catch (NumberFormatException e) {

            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        if (null != value) {
            return String.valueOf(((SocialMedia) value).getId());
        } else {
            return null;
        }
    }

}
