/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import dataModels.FestivalModel;
import dataModels.SocialMedia;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONException;
import org.primefaces.json.JSONObject;
import org.primefaces.model.UploadedFile;

/**
 *
 * @author Petar
 */
public class JsonParser {

    private JSONObject jsonObject;
    private UploadedFile file;
    private FestivalModel model;

    public JsonParser(UploadedFile file) {
        this.file = file;
        model = new FestivalModel();
    }

    public FestivalModel parse() throws FileParseException {
        InputStream stream = null;
        BufferedReader reader = null;
        try {
            stream = new ByteArrayInputStream(file.getContents());
            reader = new BufferedReader(new InputStreamReader(stream));
            StringBuilder builder = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
            String result = builder.toString();
            jsonObject = new JSONObject(result);

            parseBasicInfo();
            parseTicketInformation();
            parsePerformances();
            parseSocialNetworks();
        } catch (FileParseException ex) {
            throw ex;
        } catch (IOException | JSONException ex) {
            throw new FileParseException("There has been an error parsing the file");
        } finally {
            try {
                reader.close();
            } catch (IOException | NullPointerException ex) {
            }
            try {
                stream.close();
            } catch (IOException | NullPointerException ex) {
            }
        }
        return model;
    }

    private void parseBasicInfo() throws FileParseException {
        try {
            JSONObject fest = jsonObject.getJSONObject("Festival");
            model.setName(fest.getString("Name"));
            model.setLocation(fest.getString("Place"));
            String startDateString = fest.getString("StartDate");
            String endDateString = fest.getString("EndDate");

            DateFormat format = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
            Date startDate = format.parse(startDateString);
            Date endDate = format.parse(endDateString);
            model.setStartDate(startDate);
            model.setEndDate(endDate);
        } catch (ParseException ex) {
            throw new FileParseException("Illegal date format for start/end date");
        } catch (JSONException ex) {
            throw new FileParseException("Illegal date format for start/end date");
        }
    }

    private void parseTicketInformation() throws FileParseException {
        try {
            JSONArray tickets = jsonObject.getJSONObject("Festival").getJSONArray("Tickets");
            if (tickets.length() < 2) {
                throw new FileParseException("Not enough information for ticket prices");
            }
            int pricePerDay = Integer.parseInt(tickets.getString(0));
            int priceWholeFest = Integer.parseInt(tickets.getString(1));
            model.setPricePerDay(pricePerDay);
            model.setPriceWholeFestival(priceWholeFest);

            JSONArray availableTickets = jsonObject.getJSONObject("Festival").getJSONArray("AvailableTickets");
            if (availableTickets.length() < 2) {
                throw new FileParseException("Not enough information for amount of available tickets");
            }
            int maxPerUser = Integer.parseInt(availableTickets.getString(0));
            int numPerDay = Integer.parseInt(availableTickets.getString(1));
            model.setMaxTicketsPerUser(maxPerUser);
            model.setNumTicketsPerDay(numPerDay);
        } catch (NumberFormatException ex) {
            throw new FileParseException("Illegal tickets price/number format");
        } catch (FileParseException ex) {
            throw ex;
        } catch (JSONException ex) {
            throw new FileParseException("Error parsing ticket information");
        }
    }

    private void parsePerformances() throws FileParseException {
        try {
            JSONArray performances = jsonObject.getJSONObject("Festival").getJSONArray("PerformersList");
            for (int i = 0; i < performances.length(); i++) {
                JSONObject current = performances.getJSONObject(i);
                model.setPerformerName(current.getString("Performer"));

                String startDateString = current.getString("StartDate");
                String endDateString = current.getString("EndDate");
                String startTimeString = current.getString("StartTime");
                String endTimeString = current.getString("EndTime");

                try {
                    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
                    Date startDate = dateFormat.parse(startDateString);

                    if (startDate.before(model.getStartDate())) {
                        throw new FileParseException("Performance start date is before festival start date");
                    }

                    if (startDate.after(model.getEndDate())) {
                        throw new FileParseException("Performance start date is after festival end date");
                    }

                    DateFormat timeFormat = new SimpleDateFormat("KK:mm:ss a", Locale.ENGLISH);
                    Date startTime = timeFormat.parse(startTimeString);
                    Date endTime = timeFormat.parse(endTimeString);

                    int performanceDay = DateUtil.dateDayDiff(model.getStartDate(), startDate) + 1;

                    model.setPerformanceFestDay(performanceDay);
                    model.setStartTime(startTime);
                    model.setEndTime(endTime);
                } catch (ParseException ex) {
                    throw new FileParseException("Invalid date/time format");
                }

                model.saveCurrentPerformance();

            }
        } catch(FileParseException ex){
            throw ex;
        }catch (JSONException ex) {
            throw new FileParseException("Error parsing performances");
        }
    }

    private void parseSocialNetworks() throws FileParseException {
        try{
            List<SocialMedia> selectedSocialMedia = new ArrayList<>();
            JSONArray networks = jsonObject.getJSONObject("Festival").getJSONArray("SocialNetworks");
            for(int i = 0; i < networks.length(); i++){
                JSONObject current = networks.getJSONObject(i);
                selectedSocialMedia.add(new SocialMedia(0, current.getString("Name"), null));
                model.getSocialMediaLinks().put(current.getString("Name"), current.getString("Link"));
            }
            model.setSelectedSocialMedia(selectedSocialMedia);
        } catch(JSONException ex){
            throw new FileParseException("Error parsing social links");
        }
    }

}
