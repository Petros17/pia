/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Petar
 */
public final class PasswordUtil {

    public static String getMD5(String password) {
        try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(password.getBytes("UTF-8"));
            MessageDigest passDigest = (MessageDigest) digest.clone();
            byte[] passwordHash = passDigest.digest();
            return getString(passwordHash);
        } catch (NoSuchAlgorithmException | CloneNotSupportedException | UnsupportedEncodingException ex) {
            Logger.getLogger(PasswordUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static String getString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            byte b = bytes[i];
            String hex = Integer.toHexString((int) 0x00FF & b);
            if (hex.length() == 1) {
                sb.append("0");
            }
            sb.append(hex);
        }
        return sb.toString();
    }
    
    
    public static String validatePassword(String password) {
        String errorMessage;
        if(password.length() > 12 || password.length() < 6){
            errorMessage = "Password length must be between 6 and 12";
            return errorMessage;
        }
        
        if(!Character.isLowerCase(password.charAt(0)) && !Character.isUpperCase(password.charAt(0))){
            errorMessage = "Password must start with a letter";
            return errorMessage;
        }
        
        int numbers = 0;
        int specialChars = 0;
        int capitalLetter = 0;
        int lowerCaseLetter = 0;
        int consecutiveLetters = 0;
        
        for(int i = 0; i < password.length() && consecutiveLetters < 3; i++){
            char current = password.charAt(i);
            if(Character.isLowerCase(current)){
                lowerCaseLetter++;
                consecutiveLetters++;
                continue;
            } else if(Character.isUpperCase(current)){
                capitalLetter++;
                consecutiveLetters++;
                continue;
            }
            consecutiveLetters = 0;
            if(Character.isDigit(current)){
                numbers++;
                continue;
            }
            specialChars++;
        }
        
        if(consecutiveLetters > 2){
            errorMessage = "Maximum two consecutive letters";
            return errorMessage;
        }
        if(capitalLetter < 1){
            errorMessage = "Password must contain at least one uppercase letter";
            return errorMessage;
        }
        if(lowerCaseLetter < 3){
            errorMessage = "Password must contain at least three lowercase letters";
            return errorMessage;
        }
        if(numbers < 1){
            errorMessage = "Password must contain at least one number";
            return errorMessage;
        }
        if(specialChars < 1){
            errorMessage = "Password must contain at least one special character";
            return errorMessage;
        }
        return null;
    }
}
