/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function resized() {
    northWidth = $("#northUnit").width();
    $("#centerUnit").width(northWidth);
    if (northWidth > 980) {
        $("#westUnit").show();
        $("#centerUnit").width(northWidth - $("#westUnit").width());
    } else {
        $("#westUnit").hide();
    }
    try {
        updateLoginDialog();
    } catch (e) {
        // undefined
    }
}

function toggleMenu() {
    northWidth = $("#northUnit").width();
    $("#centerUnit").width(northWidth);
    $("#westUnit").animate({width: 'toggle'}, 200);
}

function removeExtraTable() {
    /*$(".updateableTable").children(".ui-datatable-sticky").each(function () {
     zindex = $(this).css("z-index");
     if (zindex > 10) {
     $(this).css("display", "none");
     }
     });*/
}

function handleLoginRequest(xhr, status, args) {
    if (args.validationFailed || !args.loggedIn) {
        PF('loginDialog').show();
    } else {
        PF('loginDialog').hide();
    }
}

function handleRegisterRequest(xhr, status, args) {
    if (args.validationFailed || !args.requestSent) {
        PF('signUpDialog').show();
    } else {
        PF('signUpDialog').hide();
    }
}

function handleChangePassRequest(xhr, status, args) {
    if (args.validationFailed || !args.changeSuccessfull) {
        PF('changePassDialog').show();
    } else {
        PF('changePassDialog').hide();
    }
}

function showImage(id) {
    var elem = document.getElementById(id);
    if (null !== elem) {
        elem.style.display = 'block';
    }
}

function hideImage(id) {
    var elem = document.getElementById(id);
    if (null !== elem) {
        elem.style.display = 'none';
    }
}

function viewLoadingImage(id) {
    document.getElementById(id).style.visibility = 'visible';
}

function hideLoadingImage(id) {
    document.getElementById(id).style.visibility = 'hidden';
}


function playPause(id) {
    var myVideo = document.getElementById(id);
    if (null === myVideo)
        return;
    if (myVideo.paused)
        myVideo.play();
    else
        myVideo.pause();
}

function stopVideo(id) {
    var myVideo = document.getElementById(id);
    if (null === myVideo)
        return;

    myVideo.pause();
    myVideo.load();
    myVideo.pause();
}


function reloadAllVideos() {
    $(".festVideo").each(function () {
        var thisVideo = document.getElementById($(this).attr('id'));
        thisVideo.pause();
        thisVideo.load();
    });
}

function pauseCurrent() {
    $(".festVideo").each(function () {
        var thisVideo = document.getElementById($(this).attr('id'));
        thisVideo.pause();
    });
}


function playCurrent() {
    $(".festVideo").each(function () {
        var thisVideo = document.getElementById($(this).attr('id'));
        if ($(this).is(":visible")) {
            thisVideo.play();
        }
    });
}


function onSignIn(googleUser) {
    var id_token = googleUser.getAuthResponse().id_token;
    document.getElementById('googleSigninForm:idToken').value = id_token;
    document.getElementById('googleSigninForm:googleLoginButton').click();
}

function signOut() {
    try {
        var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut();
    } catch (e) {
        console.log(e);
    }
}